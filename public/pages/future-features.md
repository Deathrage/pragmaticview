# Future features {docsify-ignore}

-   Decorators and HOCs
-   Express (register) clear after require option -> require caches imported code so restarting app is necessary to see changes in templates
-   &#9745; <strike>Mechanism to share results of async operation between class components</strike>
-   &#9745; <strike>Some kind of children helper for non-rendered children, either set of functions or wraping children in special array object when passed to component</strike>
-   &#9745; <strike>Caching of class components and custom CacheControl methods</strike>
-   &#9745; <strike>Transpilation of components from folder by ViewEngine for non TypeScript/Babel applications</strike>
-   &#9745; <strike>Singleton ViewEngine option, when true initializating anohter ViewEngine causes error</strike>
-   &#9745; <strike> onPreRender event method of class component </strike>
-   &#9745; <strike>Page layout component in viewBuilder with Placeholder component</strike>
