# Internal loader

If you do not want to use a transpiler in your application, you can use ViewEngine's template loading. The template loader is powerful enough to handle decorators, JSX and even TypeScript itself. The loader works in the same fashion as babel's or ts-node's register.

?> Express and webpack plugins uses this feature

## Basic usage

```
const path = require("path");
const ViewEngine = require("pragmatic-view").ViewEngine;

ViewEngine.register(path.resolve(__dirname, './templates'));

const Document = require('./templates/document');

let viewEngine = new ViewEngine();
let builder = viewEngine.getBuilder(Document);

builder.toString().then(html => {...});
```

ViewEngine's static method `register` needs to be called before any template is `required` (CommonJS) or `imported` (ES6+). When a path is registered, ViewEngine enhances Node.JS's `require` function with a custom loader for all imports from specified path. The path has to be absolute. The best way to get absolute path in node is to use Node's `path` module.

?> When using other transpilers (i.e. Babel for React) and ViewEngine's loader together you might consider excluding the path to templates from other transpilers as they might damage the syntax

### Typescript and PV

Either JSX or TSX (JSX with types) is usable to write templates. Loader can work with any kind of file extension (the important thing is the code inside). Usually you will rely on `.jsx` or `.tsx` but you can also use PragmaticView's own file extension `.pv`. It may help to to distinguish file that are solely meant for PragmaticView. Component files can contain ES6 imports and exports. When ES6 import/export is found it's transpiled to CommonJS as it's Node.JS's module system. Typescript syntax in TSX is plainly transformed to javascript without doing any real type checking. All of this gives programmer flexibility to chose their own style. However, it's not wise to combine all possible styles together.

!> TSX and PV files are not type checked. Typescript markup is plainly converted to Javascript without doing type checks that TSC or TS-NODE would do.

In order to distinguish whether the `.pv` file contains TypeScript or plain JavaScript a simple check is made and proper transpilation method is used. You can freely use TypeScript syntax in all of your components.

## Advanced usage

?> ViewEngine uses Babel instead of TypeScript Compiler now. Babel and it's core dependencies come together with PragmaticView.

Transpilation in ViewEngine's loader can be adjusted to some degree. ViewEngine uses Babel as it's transpiler. Babel configuration can be passed as second argument of the `register` method. You can either pass your `.babelrc` file location or manually add plugins and presets.

Plugins that are automatically added/overridden:

-   `@babel/plugin-transform-typescript` as TSX is valid also
-   `@babel/plugin-transform-modules-commonjs` as templates always have to be CommonJS
-   `@babel/plugin-transform-react-jsx` as correct `View` pragma needs to be used
-   `@babel/plugin-proposal-class-properties` as properties can be initialized on class
-   `@babel/plugin-proposal-decorators` as decorators can be used in components

**Example**

```
// Referencing workspace's babelrc
let babelOptions = {
	babelrc: path.resolve(process.cwd(), '.babelrc'),
	presets: [
		...
	],
	plugins: [
		...
	]
}

ViewEngine.register(path.resolve(__dirname, './templates'), babelOptions);
```
