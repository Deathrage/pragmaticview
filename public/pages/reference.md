# Reference

!> Information about private members may not match current implementation

## ViewEngine

Core part of the PragmaticView. Instances of `ViewEngine` are used to emit instances of [`ViewBuilder`](#viewbuilder). Constructor accepts [`ViewEngineConfig`](#viewengineconfig).

```
import { ViewEngine } from "pragmatic-view";

let viewEngine = new ViewEngine(config);
```

**Properties**\
**[`ViewEngine.prototype.config`](#viewengineconfig)**\
&emsp;&emsp;Readonly, exposes configuration of the ViewEngine.

**Methods**\
**[`ViewEngine.prototype.getBuilder(args): ViewBuilder`](#viewengineprototypegetbuilderargs)**\
&emsp;&emsp;Returns instance of new [`ViewBuilder`](#viewbuilder),
**Methods**\
**`ViewEngine.prototype.render(component: ClassComponent | FunctionComponent, context?: any): Promise<string>`**\
&emsp;&emsp;Renders specific component without emitting `ViewBuilder`.

### ViewEngine.prototype.getBuilder(args)

Accepts [`FunctionComponent`](#functioncomponent) or class [`Component`](#viewcomponent) and rendering context. Rendering context can be set only during initialization of the [`ViewBuilder`](#viewbuilder). Rendering context is available to all components in the rendering cascade. Context can be almost anything (For TypeScript it's type `Record<string,any>`).

Four possible overloads:

```
ViewEngine.prototype.getBuilder(): ViewBuilder
ViewEngine.prototype.getBuilder(root: FunctionComponent|(typeof View.Component)): ViewBuilder
ViewEngine.prototype.getBuilder(context: Object): ViewBuilder
ViewEngine.prototype.getBuilder(root: FunctionComponent|(typeof View.Component), context: Object): ViewBuilder
```

### ViewEngineConfig

**Properties**\
**`beautifyOutput`**\
&emsp;&emsp;`boolean`, beatify html before returned from [`ViewBuilder.prototype.toString()`](#viewbuilder). Default `false`.\
**`beautifyOutput`**\
&emsp;&emsp;Beautify options. Options are the same as [js-beautify](https://www.npmjs.com/package/js-beautify) options.\
**`cacheConfig`**\
&emsp;&emsp;Set's default time to live of caches. Also allows passing own store object implementing Map interface.\
**`logger: (message: string) => void`**\
&emsp;&emsp;Custom logger function for outputting rendering time. Default `console.log`.\
**`logRenderTime`**\
&emsp;&emsp;`boolean`, logs rendering time through `logger`. Default `false`.\
**`singleton`**\
&emsp;&emsp;`boolean`, initializes ViewEngine in singleton mode, initializing another ViewEngine causes an error even if new one is not set as singleton. Default `false`.\
**`layout`**\
&emsp;&emsp;Layout component, [View.Placeholder](#viewplaceholder) has to be placed inside layout

### CacheConfig

**Properties**\
**`timeToLive`**\
&emsp;&emsp;`number`, time for which a cache is stored, set in seconds.\
**`cacheMap`**\
&emsp;&emsp;`Map`, custom store object implementing [Map](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Map) interface.

## ViewBuilder

`ViewBuilder` resolves a component (including it's children) into `string` of HTML.

```
let context = {
    slug: '/jacket/male',
    customer: {
        name: 'John Doe',
        id: 1678
    }
}

let viewBuilder = viewEngine.getBuilder(MyCoponent, context);
let html = await viewBuilder.toString();
```

**Properties**\
**[`ViewBuilder.prototype.root`](#viewcomponent)**\
&emsp;&emsp;Root component of the view.\
**`ViewBuilder.prototype.layout`**\
&emsp;&emsp;Layout component, [View.Placeholder](#viewplaceholder) has to be placed inside layout.\
**`ViewBuilder.prototype.context`**\
&emsp;&emsp;Readonly, exposes config passed during ViewBuilder's initialization.

**[`private ViewBuilder.prototype.viewEngine`](#viewengine)**\
&emsp;&emsp;Reference to the parent `ViewEngine`.

**Methods**\
**`ViewBuilder.prototype.toString(): Promise<string>`**\
&emsp;&emsp;Resolves root component (including it's children) in the given context and returns parsed html as `string`.

**`private ViewBuilder.prototype._beautifyHTML(html: string): string`**\
&emsp;&emsp;Beautifies html after `_render()` if set in `ViewEngine.prototype.config`.\
**`private ViewBuilder.prototype._render(): Promise<string>`**\
&emsp;&emsp;Renders html to `string` during `toString()` method.\
**`private ViewBuilder.prototype._renderWithDuration(): Promise<string>`**\
&emsp;&emsp;Calculates time of rendering process during `toString()`.\
**`private ViewBuilder.prototype._renderLayout(page: string): Promise<string>`**\
&emsp;&emsp;Accepts [`string`] and returns layout's html including page as `string`.\
**`private ViewBuilder.prototype._renderNode(node: ViewNode): Promise<string>`**\
&emsp;&emsp;Resolves [`ViewNode`](#viewnode) to HTML.\
**`private ViewBuilder.prototype._renderIndirectChildren(children: (ViewNode|string)[]): Promise<string[]>`**\
&emsp;&emsp;Resolves children pass through `props.children` to HTML.\
**`private ViewBuilder.prototype._renderDirectChildren(node: ViewNode): Promise<string>`**\
&emsp;&emsp;Runs `FunctionComponent` or `Component.prototype.render()` and returns resolved HTML.\
**`private ViewBuilder.prototype._resolveNodeOutput(resolution:ViewNode|ViewNode[]|string[]|string): Promise<string[]>`**\
&emsp;&emsp;Accepts any kind of output that `Component` can return deciding what to do next.

## View

Pragma function and utility namespace, needs to be present in every module that contains JSX/TSX for PragmaticView. Besides that it also hosts as namespace other usefull tools and parts of PragmaticView.

**Properties**\
**[`View.Component`](#viewcomponent)**\
&emsp;&emsp;Base [`Class Component`](#viewcomponent).\
**[`View.Fragment`](#viewfragment)**\
&emsp;&emsp;Exotic component that does not output HTML.\
**[`View.Placeholder`](#viewplaceholder)**\
&emsp;&emsp;Component representing page's HTML in Layout component.\

### View.Component

Class components host advanced logic. They are capable of doing async operations such as database calls. In comparison to React there is no state. Values are directly stored as class' properties defined when extending base Component class. Promises stored as class' properties can be created in one event method (onInit) and awaited in another one (onPreRender).

```
import { View } from "pragmatic-view";

export class MyComponent extends View.Component<Props, Context> {
	private _userNamePromise: Promise<string>;
	private _userName: string;

    async onInit() {
        this._userNamePromise = fetchUsername();
    }
	async onPreRender() {
        this._userName = await _userNamePromise();
    }
    render() {
        return <span>
            You are currently visiting {this.context.slug} as {this._userName}.
        </span>
    }
    async onRender(html) {
        return html.replace('You are', 'I am');
    }
}
```

**Properties**\
**`View.Component.prototype.props`**\
&emsp;&emsp;Values of JSX/TSX tag's attributes.\
**`View.Component.prototype.context`**\
&emsp;&emsp;Rendering context, passed through [`ViewEngine.prototype.getBuilder(args)`](#viewengineprototypegetbuilderargs).\

**Methods**\
**`View.Component.prototype.onInit(): Promise<void>`**\
&emsp;&emsp;Event method. Invoked at the beginning of life cycle after class initialization. Can remove items from `props.children`.\
**`View.Component.prototype.onPreRender(html:string): Promise<string>`**\
&emsp;&emsp;Event method. Invoked after successful rendering of `props.children` of the component.\
**`View.Component.prototype.onRender(html:string): Promise<string>`**\
&emsp;&emsp;Event method. Invoked after successful rendering of whole component. Can directly modify resulting HTML string. Can be used to discard component's HTML by returning null.\
**`View.Component.prototype.render(): ViewNode`**\
&emsp;&emsp;JSX/TSX markup of the component.

### View.Fragment

Exotic element, serves as a wrap around multiple elements. Fragment itself is not transformed into HTML string. Fragments and arrays are interchangeable in this sense.

```
import { View } from "pragmatic-view";

let FuncComponent = () => {
    return <View.Fragment>
        <span>All children elements are directly returned.</span>
        <MyAwesomeComponent />
    </View.Fragment>
}

```

### View.Placeholder

In layouts Placeholder marks spot where page's HTML will be located.

```
import { View } from "pragmatic-view";

let layout = () => <html>
    <head>
        <title>Awesome layout!</title>
    </head>
    <body>
        <View.Placeholder/>
    </body>
</html>

// In ViewEngine
let config = {
    layout: layout
};

let viewEngine = new ViewEngine(config);

// In ViewBuilder
let viewBuilder = viewEngine.getBuilder(pageComponent);
viewBuilder.layout = layout;
```

Layout can be either set in [`ViewEngineConfig`](#viewengineconfig) or directly set in [`ViewBuilder`](#viewbuilder). When layout is specified in [`ViewEngineConfig`](#viewengineconfig), it will be used by all emitted [`ViewBuilders`](#viewbuilder). Setting in [`ViewBuilder`](#viewbuilder) overrides layout set in [`ViewEngineConfig`](#viewengineconfig).

### Function Component

Function component are pure functions that return JSX/TSX element. Compared to [`Class Components`](#viewcomponent) they are not designed to contain any kind of advanced logic that may use Promises or other asynchronous constructs.

```
import { View } from "pragmatic-view";

let MyAwesomeComponent = (props, context) => {
    return <div>
        <BreadCrumbs slug={context.slug}>
        <h1>{props.headline}</h1>
        <p>For simple tasks, function components suit well!</p>
    </div>
}
```

## ViewNode

Building block of virtual DOM. Represents single JSX/TSX tag.

**Properties**\
**`ViewNode.prototype.nodeConstructor`**\
&emsp;&emsp;Constructor of the JSX/TSX element. Either `string`, [`Function Component`](#functioncomponent) or [`Class Component`](#viewcomponent).\
**`ViewNode.prototype.props`**\
&emsp;&emsp;Attributes/properties set on TSX/JSX tag.\
**ViewNode.prototype.children**\
&emsp;&emsp;Either `ViewNode[]` or `string[]`. Contains elements passed between tags.

## Helpers

Collection of objects of useful functions

**Properties**\
**`Helpers.predicate`**\
&emsp;&emsp;Collection of helpers to filter out elements.\
**`Helpers.elements`**\
&emsp;&emsp;Collection of helpers to create and modify elements.\

### Helpers.predicate

Will be usually used to determine types of children.

**Methods**\
**`Helpers.predicate.isElement(obj: any) => boolean`**\
&emsp;&emsp;Returns `true` if object is valid virtual dom node, children can be dom nodes, strings, or any kind of data type.\
**`Helpers.predicate.isClassElement(obj: any) => boolean`**\
&emsp;&emsp;Returns `true` if object is valid virtual dom node with class as it's node constructor.\
**`Helpers.predicate.isFunctionElement(obj: any) => boolean`**\
&emsp;&emsp;Returns `true` if object is valid virtual dom node with function as it's node constructor.\
**`Helpers.predicate.isNativeElement(obj: any) => boolean`**\
&emsp;&emsp;Returns `true` if object is valid virtual dom node with string as it's node constructor. String node constructor means that element is a HTML element.\
**`Helpers.predicate.isRendered(obj: any) => boolean`**\
&emsp;&emsp;Returns `true` if object is actually a string. Children as string might be already rendered element or non virtual dom element.

### Helpers.elements

Allows creation and alteration of elements inside class components. Will be usually used to alter children.

**Methods**\
**`Helpers.elements.createElement(constructor: NodeConstructor, props: Props, children?: any[]) => ViewNode`**\
&emsp;&emsp;Creates and returns an element. Props and children get copied in order to prevent mutation.\
**`Helpers.elements.cloneElement(element: any) => any`**\
&emsp;&emsp;Clones an element. If element is not virtual dom node returns the element. Props and children get's copied in order to prevent mutation.\
**`Helpers.elements.extractProps(element: any) => Props`**\
&emsp;&emsp;Extracts cloned props from the element. If is not valid virtual dom node returns `null`.\
**`Helpers.elements.injectProps(element: any, props: Props) => any`**\
&emsp;&emsp;Inject props to the element. If is not valid virtual dom node returns `null` otherwise returns injected element. Injected props gets copied in order to prevent mutation. If prop is already present it's overridden. Element itself is not cloned rather mutated.

## Decorators

### Decorators collection

One of PragmaticView's named exports.

?> When using [PragmaticView template loading](/pages/no-transpil.md) decorators can be called via `@decorator` syntax by default, otherwise you may want to adjust your transpiler to work with decorators or use them as Higher Order Functions / Components

**Properties**\
**[`readonly`](#readonly-decorator)**\
&emsp;&emsp;Makes class' property readonly. After first setting cannot be change any further. Attempting to do so throws an exception.\
**[`viewStore`](#viewStore-decorator)**\
&emsp;&emsp;Marks class to receive reference of the viewStore.\
**[`cache`](#cache-decorator)**\
&emsp;&emsp;Marks class component as cachable, when component is to be rendered it's either rendered or loaded from cache. Can be used as a Higher Order Component.

### Readonly decorator

Makes class' property readonly. After first setting cannot be change any further. Attempting to do so throws an exception.

```
import { decorators } from "pragmatic-view";

class A {
	@decorators.readonly
	foo: string;

	constructor(fooValue: string) {
		this.foo = fooValue;
	}
}

let a = new A('bar');
a.foo = 'foobar'; // => throws Error

// OR

class B {
	@decorators.readonly
	foo: string;
}

let b = new B();
b.foo = 'bar';
b.foo = 'foobar'; // => throws Error
```

### ViewStore decorator

Marks class to get reference of the ViewStore. It can be used to write and read from the store. The ViewStore provides communication between components regarding data sharing.

```
// UserHeader.jsx
import { View, Decorators } from 'pragmatic-view';

@Decorators.viewStore
export class ComponentWithViewStore extends View.Component {
	async onInit() {
		if (!this.viewStore['user']) {
			this.viewStore['user'] = db.getUser(this.context.userId);
		}
		this.user = await this.viewStore['user'];
	}
	...
}

// AccountDetails.jsx
import { View, Decorators } from 'pragmatic-view';

@Decorators.viewStore
export class ComponentWithViewStore extends View.Component {
	async onInit() {
		if (!this.viewStore['user']) {
			this.viewStore['user'] = db.getUser(this.context.userId);
		}
		this.user = await this.viewStore['user'];
	}
	...
}
```

### Cache decorator

?> HTML returned from onRender method is no subject to caching. Only HTML returned from render method is cached (including props.children). Caching does not omit constructor and onRender in classes.

In a fashion similar to .NET's attributes marks component as cachable. When component is rendered for the first time (or is not already cached) the resulting HTML is saved to cache store. When component is to be rendered again it's life cycle is omitted (except constructor and onRender methods).

**`cache(constructor) => constructor`**\
&emsp;&emsp;Decorates component as cachable.
**`cache<P, C>(keyFunction?: (props: P, context: C) => string | number, timeToLive?: number ) => cache(constructor) => constructor`**\
&emsp;&emsp;Decorator factory, keyFunction creates cache key based on current props and context (for example cache key is based on userId), can override TimeToLive specified in ViewEngine config.

!> When two different components accidentally create the same cache key, their cached HTMLs can get mangled, it's always important to think about uniqueness of the key regrading components. Prefixing key with the name of the component is simple way to achieve it.
