# Helpers

PragmaticView offers a collection of useful helpers that can be utilized in order to create complex logic.

```
// ES6
import { Helpers } from "pragmatic-view";
// CommonJS
const Helpers = require("pragmatic-view").Helpers;

const { predicate, elements } = Helpers;
const { createElement } = elements;
createElement(...);
// or
Helpers.elements.createElement(...);
```

?> Helper can be de-structured into smaller blocks.

## Predicate

Collection of predicate functions can be used to determine the true nature of the children. Only one thing is certain, children that are passed to components are always in flattened array. It means that children can be handled using array methods such as map, filter, forEach etc.

?> Children are either virtual DOM nodes or anything else (mostly strings). Virtual DOM node can be made of three different node constructors classes, functions, strings (called native nodes).

All predicate methods return either `true` or `false`.

### isElement

Determines whether the child is an element. Children can be an element (virtual DOM node) or anything else.

```
...
// Finds children that are elements
onInit() {
	this.pureChildren = this.props.children.filter(Helpers.predicate.isElement);
	// Or
	this.pureChildren = this.props.children.filter(child => Helpers.predicate.isElement(child));
}
...
```

### isClassElement

Determines whether the child is an element made of [`Class Component`](/pages/reference.md#viewcomponent).

```
...
onInit() {
	const child = this.props.children.find(Helpers.predicate.isClassElement);
	// Or
	const child = this.props.children.find(child => Helpers.predicate.isClassElement(child));
	...
}
...
```

### isFunctionElement

Determines whether the child is an element made of [`Function Component`](/pages/reference.md#functioncomponent).

```
...
onInit() {
	const child = this.props.children.find(Helpers.predicate.isFunctionElement);
	// Or
	const child = this.props.children.find(child => Helpers.predicate.isFunctionElement((child));
	...
}
...
```

### isNativeElement

Determines whether the child is an element made of a string. Virtual DOM nodes that have string as own node constructor represent HTML elements. They are called native elements.

```
...
onInit() {
	const nativeChildren = this.props.children.filter(Helpers.predicate.isNativeElement);
	// Or
	const nativeChildren = this.props.children.filter(child => Helpers.predicate.isNativeElement(child));
}
...
```

### isRendered

Determines whether the child is a string. String is the result of a rendered node. Strings are also other values passed around elements in JSX, they are treated as rendered elements. Children are always already rendered in method [`onPreRender`](/pages/reference.md#viewcomponent) as they are vital to the rendering of the class itself. Children in their non-rendered state are available only in [`onInit`](/pages/reference.md#viewcomponent) method. Children passed to a function are always rendered. [More about different life cycles](/pages/lifecycle.md).

```
// Class
...
onInit() {
	const allRendered = this.props.children.every(Helpers.predicate.isRendered); // => false
	// Or
	const nativeChildren = this.props.children.filter(child => Helpers.predicate.isRendered(child)); // => false
}
...
onPreRender() {
	const allRendered = this.props.children.every(Helpers.predicate.isRendered); // => true
	// Or
	const nativeChildren = this.props.children.filter(child => Helpers.predicate.isRendered(child)); // => true
}
...

// Function
const Component = props => {
	const allRendered = this.props.children.every(Helpers.predicate.isRendered); // => true
	return (
		<div>
		...
		</div>
	);
}
```

## Elements

Element helpers provide ways to alter and create new elements.

### createElement

Takes node constructor, props and children. Children and props are copied in order to prevent outside mutation. Children will be automatically flattened.

```
Helpers.elements.createElement(nodeConstructor, props, children) => ViewNode

...
onInit() {
	this.props.children.push(Helpers.elements.createElement(MyComponent, { title: 'This component was created in onInit' }, []));
}
...
onPreRender() {
	// This component may not be rendered as only safe method for children manipulation is onInit() and could be presented as [object Object] in the result
	this.props.children.push(Helpers.elements.createElement(MyComponent, { title: 'This component was created in onPreRender' }, []));
}
...
render() {
	return (
		<div>
			// Creates an element and passes children as it's own children
			{Helpers.elements.createElement(OtherComponent, { msg: 'This component was created in render' }, ...this.props.children)}
		</div>
	);
}

```

### cloneElement

Takes element and creates it's 1:1 copy. If passed element is not valid element it will be returned as such. Children accessed in other methods than [`onInit`](/pages/reference.md#viewcomponent) are not valid elements as are already rendered. Props and children are copied in order to prevent future mutation.

```
onInit() {
	const clone = Helpers.elements.cloneElement(this.titleElement);
	// Could take JSX directly
	const clone = Helpers.elements.cloneElement(<SomeComponent />);
}
```

### extractProps

Takes element and return cloned props. If invalid element is passed undefined is returned. Props can be extracted only in method [`onInit`](/pages/reference.md#viewcomponent).

```
onInit() {
	const props = Helpers.elements.extractProps(this.props.children[0]);
	console.log(`This component has key ${props.key}`);
}
```

### injectProps

Extends element's props and overrides already present ones. If invalid element is passed undefined is returned. Props can be injected only in method [`onInit`](/pages/reference.md#viewcomponent).

```
onInit() {
	Helpers.elements.injectProps(this.props.children[0], { title: 'This props was injected!' });
}
```
