# Component caching

Web pages may contain data that won't change very often. There are two possible ways how to handle it. Persistent data can be gathered before the rendering and passed inside context. However, this might cause issue as adding or removing component of the view also requires adjusting the context. Second way is to use PargmaticView's in-build caching and perform request for the data inside the components. As the component create a cache of itself it's logic won't get re-run in future renders unless the cache runs out of it's time to live.

## Basic usage

There is a property in [`ViewEngine's config`](/pages/reference.md#viewengineconfig) called cacheConfig. It defines default behavior of the caches. It is also possible to pass own storage object that implements [Map interface](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Map). Custom Map object can serve as bridge between PragaticView and different caching solutions.

Caches exists by default within the ViewEngine object and are consumed or created by emitted ViewBuilders. This means that multiple ViewEngines do not share caches among each other (however they can be given common Map object).

```
import { CacheConfig, ViewEngine } from "pragmatic-view";

const cacheConfig: CacheConfig = {
	timeToLive: 60;
}

const viewEngine = new ViewEngine({
	cacheConfig
});

```

TimeToLive can be set either in the config or through [`cache decorator`](/pages/reference.md#cachedecorator). TimeToLive set through decorator overrides time set in config. If no TimeToLive is set, caches have default time of 3600 seconds.

?> It's a standard setting time to live in second. The PragmaticView cache's TimeToLive is set in second too.

PragmaticView exports [`decorators collection`](/pages/reference.md#decorators-collection) object that hosts different useful decorators. One of them is decorator named cache that marks component as cachable.

?> Decorator can be either used with decorator syntax `@decorator` or as Higher Order Function / Component `decorator(function)` or `decorator(...args)(function)` for decorator factories.

```
import { View, Decorators } from "pragmatic-view";
import UserInfo from "./types/UserInfo";
import Context from "./types/Context";
import db from "./db";

const { cache } = decorators;

@cache
class UserProfile extends View.Component<{}, Context> {
	private userInfoPromise: Promise<UserInfo>;
	private userInfo: UserInfo;

	async onInit() {
		this.userInfoPromise = db.getUserInfo(this.context.userId);
	}

	async onPreRender() {
		this.userInfo = await this.userInfoPromise;
	}

	render() {
		...
	}
}
```

## Advanced usage

Cache decorator can be called either as decorator or decorator factory. If used as decorator the default (or from ViewEngine config) TimeToLive is used. Also the component's constructor is used as cache key. That means every use of the component shares the cache. If you want to specify custom key in order to distinguish different usages user decorator factory.

Cache decorator factory takes up to two arguments. First argument is function that takes component's current props and context to create unique cache key. Second argument is custom TimeToLive. If only second argument is passed (first is passed as null) component constructor is used as key. It's same as when used as decorator (but you can set custom TimeToLive). Decorator company allows to bind caches to varying information such as user id passed through props or context.

```
// as decorator
@cache
class UserDialog extends View.Component { ... }

// as decorator factory
@cache((props, context) => `userId${context.userId}`)
class UserDialog extends View.Component { ... }

```

When component is cached without key it's constructor is used as the key. It means that every usage of the component shares the same cache. If you want to relate caches to specific variable such as user id or want to use the component in different context it's wise to generate custom key through key function of the decorator. **Returning `false` as key turns caching off.**

!> PragmaticView's decorators follow stage 1 specification of decorator proposal

If your stack doesn't allow decorator to be used, almost all PragmaticView's decorators can be used as Higher Order Functions. [PragmaticView's inbuilt template loader](/pages/no-transpil.md) takes care of decorators. The decorators do not mutate the function itself, they just reference the function to underlying parts of PragmaticView, this makes them more similar to .NET's attributes.

```

class Foo extends ......

export default cache((props, context) => `uniqueIdentifier`)(Foo);
// or
export default cache(Foo);
```

## Effect on component's life cycle

The fact that component is marked as cachable doesn't mean that it's life cycle is omitted. Cachable component (that has living cache) goes through simplified life cycle where methods onInit, onPreRender and render are omitted. Constructor of class is invoked and method onRender is invoked too. Html passed to onRender method is the cached one. **Result of onRender method is not cached.**

Descendant components are not initialized as their HTML including result of onRender method (if class component) is inside cachable components cache. If cachable component's descendant is also cachable and it's time to live is shorter than parent's, it's cache gets renewed after the parent's cache exceeds it's lifespan as descendant component is not run as log as it's part of parent's alive cache.

Theoretically function component can be marked as cachable too. If they are marked as cachable their inner logic is omitted altogether as they are equivalent to class' render method.
