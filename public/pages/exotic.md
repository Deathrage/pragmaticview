# Exotic components

The View besides being pragma and having Component class offers two exotic components. They are called exotic as they are not real components. They simply work as a flag for the resolver to perform something.

## Placeholders and layouts

Components do not have to include whole page itself. In order to achieve some kind of modularity there are layouts. Layout is a component that includes `View.Placeholder` component where the placeholder marks the spot of subsequent HTML.

```
let layout = (props, context) => <html>
    <head>
		<MyAwesomeMeta />
		<MyAwesomeStyles />
        <title>{context.title}</title>
    </head>
    <body>
        <View.Placeholder/>
    </body>
</html>;

```

Layout components have limitations in a way that they cannot be given props. They can only read data from context object or viewStore object.

?> Layout is re-rendered for every view

Layouts can be set globally through [`ViewEngineConfig`](/pages/reference?id=viewengineconfig). This setting can be subsequently overridden by specifying new layout through ViewBuilder.

```
// Global layout
let config = {
    layout: layout
};

let viewEngine = new ViewEngine(config);

// Overriding through ViewBuilder
let viewBuilder = viewEngine.getBuilder(pageComponent);
viewBuilder.layout = layout;

```

## Fragments

Fragments are a way to wrap siblings in order to be returned by a component. Fragments themselves do not exist as they are not included in the resulting html.

```
import { View } from 'pragmatic-view';

export const Component = props => {
	....
	return (
		<View.Fragment>
			Hello from the fragment!
			<OneComponent />
			<AnotherComponent />
		</View.Fragment>
	);
}

```
