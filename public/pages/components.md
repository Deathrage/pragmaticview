# Components

As was already said there are two approaches to writing a component classes and functions.

## Functions

They are meant to be simple view components. The are simple regarding their inner logic as they are only supposed to return markup or do some simplified logic. They cannot and should not try to access any kind of data outside the view itself through asynchronous means (i.e. use only props, context or import synchronous code). If you are already providing everything necessary through these means (mostly through context) there is no reason to build any classes.

```
import { View } from 'pragmatic-view';
// Helper to merge classes into one string
import classNames from 'classnames';

export const Button = ({ color, children }) => {
	return (
		<button className={classNames('btn', `btn-${color}`)}>
			{children}
		</button>
	);
}
```

## Classes

They are meant to host complex logic that has to utilize asynchronous code such as doing request to database or opening a file from the disk. In order to do so they provide three different life cycle methods that can return `Promise` and it will be awaited before continuing. Classes can benefit through other utilities such as decorators. Decorators provide tools such as caching or viewStore (special storage withing single view that can be used to share data across all components within the view).

?> Decorator can he hypothetically used with Functions as Higher Order Functions (though TypeScript compiler might object)

```
import { View, Decorators } from 'pragmatic-view';
import { userProvider } from './db/userProvider';

// In order to have viewStore property, class has to be decorated
@Decorators.viewStore
// All class components have to extends View.Component abstract class
class UserProfile extends View.Component {
	// viewStore can be used to share fetch promises across components thus limiting need for additional requests
	onInit() {
		// If no other component already created and exposed fetch for user, create it and expose it
		if (!this.viewStore['userPromise']) {
			this.viewStore['userPromise'] = userProvider.getUser(this.context.userId);
		}
	}

	async onPreRender() {
		// await exposed fetch promise, several components can await a promise simultaneously
		this.user = await this.viewStore['userPromise'];
		this.doSomethingWithUser();
	}

	doSomethingWithUser() {
		....
	}

	// Similarly to React all classes have to implement render method
	render() {
		return (
			....
		);
	}
}
```

If there is a chance that more components have to get same data it's wise to share it through the viewStore. It's already important to schedule what each event method is to do as they are invoked in very different phases of the component's life cycle. [About components' life cycle.](/pages/lifecycle.md)

Overall classes provide three event methods:

-   `onInit() => void | Promise<void>` that is invoked after the component enters rendering cycle
-   `onPreRender() => void | Promise<void>` that is invoked after children (props.children) are successfully rendered
-   `onRender(html: string) => string | Promise<string>` that is invoked after the content of render method is successfully rendered
