# Changelog

## 1.0.0 {docsify-ignore}

-   Released!
-   Default export `View` changed to a named export of the package
-   `ViewStore` added
