# PragmaticView Loader

<img src="http://lukasprochazka.net/img/loader.png" alt="PragmaticView Loader" width="180" />

PragmaticView can be used to build static pages through webpack using [PragmaticView loader](https://www.npmjs.com/package/pragmatic-view-loader). In it's core the loader is one [ViewEngine](http://pragmaticview.lukasprochazka.net/#/pages/reference?id=viewengine) that emits [ViewBuilder](http://pragmaticview.lukasprochazka.net/#/pages/reference?id=viewbuilder) for each loaded file. Imported components in the loaded files are transpiled on the fly by [ViewEngine's require hook](http://pragmaticview.lukasprochazka.net/#/pages/no-transpil).

## Using the loader

```
npm install --save pragmatic-view pragmatic-view-loader
```

When writing a template that is directly processed by webpack should be exported either as default in ES6 or directly in CommonJS. This is caused by fact that webpack references processed templates by their location address. It makes impossible for ViewEngine to extract named exports.

```
// Proper way to export in ES6
export default () => <div>Awesome Component</div>;

// Proper way to export in CommonJS
module.exports = () => <div>Awesome Component</div>;
```

Components imported in these templates can be imported any possible way.

?> Don't be afraid of importing helper methods or variables from non-template files. The way templates are evaluated prevents webpack from any kind of problematic behavior.

?> `.pv` is an extensions created specially for PragmaticView templates in order to distinguish them from other `.jsx` and `.tsx` files that are not meant to be used by PragmaticView. PV file can either contain typescript or javascript. It's essentially just an alias for `.jsx` and `.tsx`.

Loader processes either templates as webpack entries or templates import withing a webpack entry. Loader can be associated with `.jsx`, `.tsx` or `.pv` extensions. Path with templates should be ignored by non-related loaders. Place your templates into special folder in order to easily distinguish them from other files.

```
// webpack.config.js
module.exports = {
    ...
    module: {
        rules: [
            {
                test: /\.js$/i,
                exclude: /(node_modules|templates)/,
                loader: 'babel-loader'
            },
            {
                test: /\.(tsx|jsx|pv)/,
                include: /templates/,
                loader: 'pragmatic-view-loader"
            },
        ]
    }
}
```

This simple use case is not enough. It would just add template's html in strings to the .js bundle. You might want to add additional loaders to extract the strings.

PragmaticView loader can be passed options. If you are using layouts or imports in your templates it might me necessary to set `templateDir` option.

Possible options:

-   `templateDir` - directory of templates, necessary if you are importing components
-   `layout` - common layout for templates, usually rest of page including `<head>`
-   `context` - data shared to all components, same as [context in ViewBuilder](http://pragmaticview.lukasprochazka.net/#/pages/reference?id=viewbuilder)
-   `registerOptions` - additional Babel config, [about register](http://pragmaticview.lukasprochazka.net/#/pages/no-transpil)

## Extracting the HTML

Output of the pragmatic loader is the same as when `.html` file is loaded. This means it can be passed to other loader that work with `.html` files such as [html-loader](https://github.com/webpack-contrib/html-loader).

In order to extract file you might want to use series of [html-loader](https://github.com/webpack-contrib/html-loader) (loads files referenced in html such as src of img), [extract-loader](https://github.com/peerigon/extract-loader), [file-loader](https://github.com/webpack-contrib/file-loader) (emits html files).

```
// using multiple loaders
module.exports = {
    ...
    module: {
        rules: [
            {
                test: /\.js$/i,
                exclude: /(node_modules|templates)/,
                loader: 'babel-loader'
            },
            {
                test: /\.(tsx|jsx|pv)/,
                include: /templates/,
                use: [
                    'file-loader?name=[name].html',
                    'extract-loader',
                    'html-loader',
                    {
                        loader: 'pragmatic-view-loader',
                        options: {
                            templateDir: path.resolve(__dirname, './templates')
                        }
                    }
                ]
            },
        ]
    }
}
```

?> Don't forget to specify `.html` file extension when emiting templates from webpack, otherwise they will be given the same extension they had at the beginning.
