# Getting started

?> PragmaticView can be used without any kind of transpiler thanks to it's register method. [Learn more!](/pages/no-transpil.md)

```
npm install --save pragmatic-view
```

## Configuring workspace

In many ways PragmaticView is similar to React as it utilizes React's language sugar known as JSX. As JSX is not standard language feature it has to be turned in JavaScript before running. There are essentially two ways how to implement transpilation of the source code. You can either use third party transpiler such as Babel or use PragmaticView's internal loader.

[Using Babel or TypeScript Compiler](/pages/typescript.md)\
[Using internal loader](/pages/np-transpil.md)

```
// Using internal loader

// index.js
const path = require("path");
const ViewEngine = require("pragmatic-view").ViewEngine;

ViewEngine.register(path.resolve(__dirname, './templates'));

// It's important to register the loader before any template is imported, best place is the top of index file
// register() is a require hook similar to babel/register or ts-node/register

const Homepage = require('./templates/Homepage');
```

When using internal loader you can choose wide range of options as it is even capable of transpiling TypeScript. When writing templates for the loader you can use CommonJS or ES6+ module system. The loader is quite powerful and is capable of resolving a lot of different styles. [All capabilities of the internal loader](/pages/no-transpil.md#advanced-usage).

?> From now on all examples will use ES6+ module syntax.

```
//CommonJS
const { View } = require('pragmatic-view');
// or
const View = require('pragmatic-view').View;

// ES6+
import { View } from 'pragmatic-view';
```

## Creating simple component

Similarly to React there are two kinds of components functions and classes. Functions are simple parts of views that are to be recycled multiple times. Classes can host advanced logic. Their life cycle methods are `async`. That makes classes capable of calling other parts of the application that use `Promises` or perform http requests. Usually majority of HTML will be outputed by functions while classes will organize them and inject them with data.

```
// HelloWorld.js
import { View } from 'pragmatic-view';

export const HelloWorld = props => <div>Hello world!</div>;

// somewhere else
import { ViewEngine } from 'pragmatic-view';
ViewEngine.register(pathToComponents);

import { HelloWorld } from './components/HelloWorld';

const viewEngine = new ViewEngine();
viewEngine.render(HelloWorld).then(console.log) // => '<div>Hello world</div>';
```

As PragmaticView uses JSX it follows similar patterns to React and preact. In order for component to work a pragma has to be present withing the scope. It can be either imported on the top of the file or assigned as global variable. PragmaticView's pragma is called `View` and it's one of the four major objects that PragmaticView exports.

?> Props of native elements (div, span, img, h1...) corresponds to their HTML counterparts. The only exception is prop `className` as the word `class` is reserved keyword in JavaScript. Props can use characters such as `-` in `data-` attributes. Prop names should follow lower camelCase and will be transformed to lowercase during render.

```
// UserProfile.js
import { View } from 'pragmatic-view';
import { db } from './db';

import Avatar from './Avatar';
import UserBox from './UserBox';

export class UserProfile extends View.Component {
	async onInit() {
		this.user = await db.fetchUser(this.context.userId);
	}

	render() {
		return (
			<div className="user-profile__wrap">
				<h1 className={this.props.headerClassName}>Welcome {this.user.userName} to your profile page!</h1>
				<Avatar src={this.user.profileImage} className="user-profile__avatar" />
				<UserBox user={this.user} />
				<button type="button" className="user-profile__signout" onclick="signout()">Sign out</button>
			</div>
		);
	}
}

// somewhere else in pseudo MVC router
import viewEngine from './rendering';
import createContext from './helpers';
import { UserProfile } from './components/UserProfile';

export const handleUserRequest = (request, response) => {
	const context = createContext(request);
	viewEngine.render(UserProfile, context).then(response.send);
}
```

The example above is more complex. It demonstrates how to serve HTML through imaginary MVC framework (could be ExpressJS? [There is a plugin for it!](/pages/express.md)).

## Basic philosophy

PragmaticView consists of three major parts Components, ViewEngine and ViewBuilder. Let's ignore components for now.

Class **ViewEngine** is the core of the application. It is used to render views. ViewEngine can be given a config object. Best practice is having single ViewEngine as a singleton object.

```
import { ViewEngine } from 'pragmatic-view';

const config = {
	beautifyOutput: true,
	singleton: true,
	layout: LayoutComponent
}

export const viewEngine = new ViewEngine(config);
```

Great use of the config is setting a layout for each view. Resulting HTML will always include this layout. Layout components work as any other components and thus are able to consume context or viewStore objects.

**ViewBuilder** is special object. It's never initialized directly rather through the ViewEngine. ViewBuilder object is wrapper around the view itself (it's amalgamation of components). In order to get the string output, method `toString` has to be called. The step with ViewBuilder can be skipped by using ViewEngine's method `render` that handles ViewBuilder internally. It might be useful to expose ViewBuilder through ViewEngine's method `getBuilder` as it can be used to override layout in specific situations. Currently `layout` is only property that can be overridden as it is directly inherited from the ViewEngine.

```
import viewEngine from './OtherPartOfApplication';
import Component from './components/Component';
import Layout from './components/layouts/Layout';

const context = {
	foo: 'bar'
}

const viewBuilder = viewEngine.getBuilder(Component, context);
viewBuilder.layout = Layout;
viewBuilder.toString().then(doSomethingWithResult);

// Without viewBuilder
viewEngine.render(Component, context).then(doSomethingWithResult);
```

## Components

As was already said there are two approaches to writing a component classes and functions.

**Functions** are meant to be simple view components. The are simple regarding their inner logic as they are only supposed to return markup or do some simplified logic.

**Classes** are meant to host complex logic that has to utilize asynchronous code such as doing request to database or opening a file from the disk.

[More about the differences between functions and classes.](/pages/components.md)
