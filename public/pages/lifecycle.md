# Components' lifecycle

As stated in previous chapter they are two ways how to write components and there is drastic difference in their behavior withing the view. Currently in other JSX using frameworks notably React there is a blurred line between capabilities and use cases of the two approaches (especially thanks to hooks). PragmaticView tries to draw a solid line between these two approaches.

## Function Component

Functions work on simple input/output principle. They are expected to be as much pure as possible. Pure functions are those who always return same result with same arguments. They should never use anything that relies on asynchronous code and Promises.

![function-lifecycle](../func-lifecycle.png 'function-lifecycle')

Functions are unable to adjust children in their "non-rendered" state nor are they able to alter resulting html. They also cannot benefit from helpers such as cache or viewStore (however, it's hypothetically possible to use them as classic Higher Order Functions).

## Class Component

Class Components are the powerhouse of the PargmaticView. They can host advanced logic. Due to event methods being async they can perform async calls and await their results. Remember that awaiting async calls stops rendering of the child layer. However, it does not limit rendering of siblings or higher layers.

?> Event methods of the base class are optional (.NET developer would call them virtual, it's not exactly the same thing though as virtual methods can have body describing their default behavior, on the other hand it's wrong to call them abstract as the implementation is merely optional. When method is not implement it simply won't be called). There is only one abstract method (required to be implemented) and that's render()

![class-lifecycle](../class-lifecycle.png 'class-lifecycle')

## Resolver

Both flowcharts contain something called "Resolver". It's a name for the inner parts of [`ViewBuilder`](/pages/reference.md#viewbuilder) that takes care of rendering all components. Main purpose of the resolver is to lead a component through it's lifecycle, consume it's output and decide what to with it. It consume anything that component creates returning a string (uses `String()` on non-component children) in the end that represents the component and any other components (their virtual DOM nodes) that descends from the component.
