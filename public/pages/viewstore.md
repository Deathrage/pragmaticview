## ViewStore

ViewStore is a special object that can be used to store information across all components withing a view. In order to keep things simple not all components can read and write to viewStore. ViewStore is accessed by decorating the class by decorator `viewStore`. It can be found within the `Decorators` collection.

```
import { View, Decorators } from 'pragmatic-view';

@Decorators.viewStore
export class ComponentWithViewStore extends View.Component {
	onInit() {
		this.viewStore['foo'] = 'bar;
	}
	...
}
```

ViewStore is exposed through mutable reference. Information stored within the viewStore can be accessed by any component.

?> If you are using TypeScript the interface of your viewStore is the third generic argument of `View.Component`, the previous two are props and context.

```
// UserHeader.jsx
import { View, Decorators } from 'pragmatic-view';

@Decorators.viewStore
export class ComponentWithViewStore extends View.Component {
	async onInit() {
		if (!this.viewStore['user']) {
			this.viewStore['user'] = db.getUser(this.context.userId);
		}
		this.user = await this.viewStore['user'];
	}
	...
}

// AccountDetails.jsx
import { View, Decorators } from 'pragmatic-view';

@Decorators.viewStore
export class ComponentWithViewStore extends View.Component {
	async onInit() {
		if (!this.viewStore['user']) {
			this.viewStore['user'] = db.getUser(this.context.userId);
		}
		this.user = await this.viewStore['user'];
	}
	...
}
```

In the example above `Promise` fetching user data is shared through viewStore as both components needs the same data. The request will be only sent by the first component as the second component will already find the Promise in the viewStore.
