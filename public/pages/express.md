# Express plugin

<img src="http://lukasprochazka.net/img/express.png" alt="PragmaticView Loader" width="250" />

```
npm install --save pragmatic-view pragmatic-view-express
```

## Adding to Express

Adding custom templating engine to Express is quite inconvenient. That's why PragmaticView offers helper method that takes care of everything. Just pass express app to the helper method.

?> Everything withing Express is automatically adjusted thus you can immediately use PragmaticView in your controllers through Express' `response.render` method.

```
// ES6
import pragmaticView from 'pragmatic-view-express';
// CommonJS
const pragmaticView = require('pragmatic-view-express').default;

// creating express app
const app = express();

pragmaticView(app);

app.get('/', (req, res) => {
	res.render('home');
});

app.listen(8080);
```

You may want to pass config as additional argument.

?> `templateDir` set's the path to Express' view lookup mechanism, so you do not need to manually specify `app.set('views', ...)`

Config:

-   `templateExtension` extension of template files (`.jsx`, `.tsx` or `.pv`), default `.pv`
-   `templateDir` directory of templates
-   `defaultLayout` path to default layout template, relative is evaluated according to `templateDir`
-   `registerOptions` additional template transpilation options, [learn more](http://pragmaticview.lukasprochazka.net/#/pages/no-transpil?id=advanced-usage)

```
pragmaticView(app, {
	templateDir: path.resolve(process.cwd(), './views'),
	templateExtension: '.jsx'
});
```

## Rendering templates

Templates are rendered through express' `response.render` method that accept two argument. First argument is relative path to the template without extension (relative to Express' template directory set either in `templateDir` or `app.set('views', './directory')`). Second argument is either `context` object or `options` object.

```
app.get('/aboutus', (req, res) => {
	let context = {
		title: 'My page'
	}
	res.render('home', context);
});
```

`options` object can include

-   `layout` relative path (works like path to template), if `false` is passed instead of string, no layout is used
-   `context` context object

```
// Rendering with different layout
app.get('/aboutus', (req, res) => {
	let context = {
		title: 'My page'
	}
	res.render('home', {
		layout: 'bluelayout',
		context: {
			title: 'My page'
		}
	});
});

// Rendering without layout
app.get('/aboutus', (req, res) => {
	let context = {
		title: 'My page'
	}
	res.render('home', {
		layout: false,
		context: {
			title: 'My page'
		}
	});
});
```
