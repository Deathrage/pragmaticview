# TypeScript & Babel integration

JSX syntax is not native to JavaScript and needs to be transpiled. If your app is using either Typescript (and it's TSCompiler) or Babel you might want to set JSXFactory/pragma that transforms JSX into `View` functions.

?> Function that JSX syntax represents is usually called jsxFactory in Typescript or pragma in other transpilers.

## TypeScript Compiler

Typescript can be used either as pre-run transpiler or runtime transpiler thanks to TS-NODE, an extension that transpiles TypeScript on demand during runtime.

Typescript is usually configured in tsconfig.json. Within it's `compilerOptions` there are two properties `jsx` and `jsxFactory`.

```
// tsconfig.json
{
	"compilerOptions": {
		"jsx": "react",
		"jsxFactory": "View",
	}
}
```

?> Turn `experimentalDecorators` on to use PragmaticView's [decorators](/pages/reference.md#decorators-collection) as decorators instead of higher order functions

TypeScript Compiler either transpiles JSX or leaves it alone. When `jsx` is set to `react` TSC transpiles JSX to specific jsxFactory (pragma) function. Property `jsxFactory` defines the jsxFactory function.

## Babel

If you want to use JSX with babel you have to install `@babel/plugin-transform-react-jsx` plugin.

?> If you want to use decorators like [`cache decorator`](/pages/cache.md) add `@babel/plugin-proposal-decorators`

```
// .babelrc
{
  "plugins": [
    ["@babel/plugin-transform-react-jsx", {
      "pragma": "View",
    }]
  ]
}
```

Babel's plugin transpiles JSX to React's pragma by default. If you want to use different pragma specify `pragma` option to `View`.
