<!-- _coverpage.md -->

![logo](/logo.png 'PragmaticView')

# PragmaticView

> JSX/TSX templating engine

-   React and ASP.NET inspired syntax
-   Usable without a transpiler!
-   1.0.0

[GitLab](https://gitlab.com/Deathrage/pragmaticview)
[Get Started](/pages/getting-started.md)

<!-- background color -->

![color](#f0f0f0)
