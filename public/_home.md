?> This framework tries to take different approach to server-side templating.

# Why? {docsify-ignore}

There is plenty of server-side templating engines for Node. Their common trait is being based on string literals. There are no templating engines that use complex view components similar to React's or Angular's. Other platforms such as ASP.NET offer these kind of complex building block. PragmaticView uses React's JSX in order to connect HTML-like markup with component's codebehind. **Using JSX means that components are JavaScript modules and as such can import values, functions, methods, objects, classes from other modules.**

```
npm install --save pragmatic-view
```

**[Get started](/pages/getting-started.md)**
