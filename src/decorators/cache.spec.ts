import { expect } from 'chai';
import { Component } from '../View';
import { SubscriptionManager } from '../subscriptions/SubscriptionManager';
import { SubscriptionEnum } from '../subscriptions/SubscriptionsEnum';
import { cache, CacheRecordData } from './cache';

describe('Test cache decorator', () => {
	it('Should subscribe component to cache', () => {
		@cache
		class CompA extends Component {
			render() {
				throw new Error('Method not implemented.');
			}
		}
		expect(SubscriptionManager.isSubscribeTo(CompA, SubscriptionEnum.CACHE)).to.equal(true);
	});

	it('Should subscribe component to cache using empty factory', () => {
		@cache()
		class CompB extends Component {
			render() {
				throw new Error('Method not implemented.');
			}
		}
		expect(SubscriptionManager.isSubscribeTo(CompB, SubscriptionEnum.CACHE)).to.equal(true);
	});

	it('Should subscribe component to cache using filled factory', () => {
		@cache(props => 'foo', 5000)
		class CompB extends Component {
			render() {
				throw new Error('Method not implemented.');
			}
		}
		const _data = SubscriptionManager.getSubscriptionData<CacheRecordData>(
			CompB,
			SubscriptionEnum.CACHE
		) as CacheRecordData;
		expect(_data.resolveKey({}, {})).to.equal('foo');
	});
});
