import { describe } from 'mocha';
import { expect } from 'chai';
import { readonly } from './readonly';

describe('Readonly decorator', () => {
	it('Should throw when reassign initialized value', () => {
		class Foo {
			@readonly
			value: string = 'gumba';
		}
		const foo = new Foo();
		expect(function() {
			foo.value = 'bar';
		}).to.throw();
	});
	it('Should throw when reassign not throw when assigned not initialized value', () => {
		class Foo {
			@readonly
			value: string;
		}
		const foo = new Foo();
		expect(function() {
			foo.value = 'bar';
		}).not.to.throw();
	});
	it('Should throw when reassign not initialized value thas was assigned value', () => {
		class Foo {
			@readonly
			value: string;
		}
		const foo = new Foo();
		foo.value = 'foo';
		expect(function() {
			foo.value = 'bar';
		}).to.throw();
	});
});
