/* eslint-disable import/export */

import { ClassComponent } from '../View';
import { SubscriptionManager } from '../subscriptions/SubscriptionManager';
import { SubscriptionEnum } from '../subscriptions/SubscriptionsEnum';
import { isClass } from '../helpers/predicate/isClass';

const doSubscribe = (constructor: ClassComponent) => {
	void SubscriptionManager.subscribe<null>(constructor, SubscriptionEnum.VIEW_STORE);
	return constructor;
};

export function viewStore();
export function viewStore(constructor: ClassComponent);
export function viewStore(firstArgument?: any) {
	if (isClass(firstArgument)) return doSubscribe(firstArgument);
	return (constructor: ClassComponent) => doSubscribe(constructor);
}
