import { expect } from 'chai';
import { Component } from '../View';
import { SubscriptionEnum } from '../subscriptions/SubscriptionsEnum';
import { SubscriptionManager } from '../subscriptions/SubscriptionManager';
import { viewStore } from './viewStore';
import { cache } from './cache';

describe('Test view store decorator', () => {
	it('Should subscribe component to viewStore', () => {
		@viewStore
		class CompA extends Component {
			render() {
				throw new Error('Method not implemented.');
			}
		}
		const _result = SubscriptionManager.isSubscribeTo(CompA, SubscriptionEnum.VIEW_STORE);
		expect(_result).to.equal(true);
	});
	it('Should work combined with cache decorator', () => {
		@viewStore
		@cache
		class CompA extends Component {
			render() {
				throw new Error('Method not implemented.');
			}
		}
		expect([
			SubscriptionManager.isSubscribeTo(CompA, SubscriptionEnum.VIEW_STORE),
			SubscriptionManager.isSubscribeTo(CompA, SubscriptionEnum.CACHE),
		]).to.eql([true, true]);
	});
	it('Should work combined with cache decorator in reverse order', () => {
		@viewStore
		@cache
		class CompA extends Component {
			render() {
				throw new Error('Method not implemented.');
			}
		}
		expect([
			SubscriptionManager.isSubscribeTo(CompA, SubscriptionEnum.VIEW_STORE),
			SubscriptionManager.isSubscribeTo(CompA, SubscriptionEnum.CACHE),
		]).to.eql([true, true]);
	});
	it('Should work combined with cache decorator and keyFunction', () => {
		@viewStore
		@cache(() => 'foo')
		class CompA extends Component {
			render() {
				throw new Error('Method not implemented.');
			}
		}
		expect([
			SubscriptionManager.isSubscribeTo(CompA, SubscriptionEnum.VIEW_STORE),
			SubscriptionManager.isSubscribeTo(CompA, SubscriptionEnum.CACHE),
		]).to.eql([true, true]);
	});
});
