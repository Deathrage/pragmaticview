const throwError = (key: string, object: Object) => {
	throw new Error(`Cannot reassign property ${key} of object ${object} because it's set as readonly.`);
};

export function readonly(target: any, key: string, descriptor: PropertyDescriptor = {}) {
	const _protoValue = target[key];

	Reflect.defineProperty(target, key, {
		...descriptor,
		get: () => _protoValue,
		set: function(initValue) {
			if (this === target) return;
			if (_protoValue) throwError(key, this);

			let _value = _protoValue || initValue;
			const _oldDescriptor = Reflect.getOwnPropertyDescriptor(this, key);
			Reflect.defineProperty(this, key, {
				..._oldDescriptor,
				get: () => _value,
				set: nextValue => {
					if (_value) throwError(key, this);
					_value = nextValue;
				},
			});
		},
	});
}
