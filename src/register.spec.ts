import { expect } from 'chai';
import { normalizeSlashes, checker } from './register';

describe('Test register related features', () => {
	it('Normalize slashes', () => {
		expect(normalizeSlashes('weird\\path\\file.ts')).to.equal('weird/path/file.ts');
	});

	it('Test checker', () => {
		let checkFunc = checker('path/to');
		expect(checkFunc('path/to/match.ts')).to.equal(true);
	});
});
