import { addHook } from 'pirates';
import * as babel from '@babel/core';
import cloneDeep = require('clone-deep');

export interface RegisterConfig extends babel.TransformOptions {
	onlyPVFiles?: boolean;
}

export const normalizeSlashes = (value: string) => {
	return value.replace(/\\/g, '/');
};

const defaultConfig: babel.TransformOptions = {
	plugins: [
		[
			'@babel/plugin-transform-modules-commonjs',
			{
				allowTopLevelThis: true,
			},
		],
		[
			'@babel/plugin-transform-react-jsx',
			{
				pragma: 'View',
			},
		],
		[
			'@babel/plugin-proposal-class-properties',
			{
				loose: true,
			},
		],
		[
			'@babel/plugin-proposal-decorators',
			{
				legacy: true,
			},
		],
		[
			'@babel/plugin-transform-typescript',
			{
				isTSX: true,
				jsxPragma: 'View',
			},
		],
	],
};

export const prepareConfig = (passedConfig?: babel.TransformOptions): babel.TransformOptions => {
	let clone = cloneDeep(passedConfig);

	let plugins = clone.plugins || [];
	clone = {
		...clone,
		plugins: plugins.concat(defaultConfig.plugins),
	};

	return clone;
};

export const checker = (pth: string) => (filename: string) => {
	let index = filename.indexOf(pth);
	return index > -1;
};

export const transpile = (config: babel.TransformOptions) => (content: string, filename: string) => {
	let result = babel.transform(content, config);
	return result.code;
};

export const register = (pth: string, config?: RegisterConfig) => {
	let babelConfig = prepareConfig(config || {});

	// Create transpiler and mathcer
	let matcher = checker(pth);
	let transpiler = transpile(babelConfig);

	// Decied what extensions are to be resolved
	let extensions = ['.jsx', '.tsx', '.pv'];
	if (config && config.onlyPVFiles === true) {
		extensions = extensions.filter(ex => ex === '.pv');
	}

	// add hook
	addHook(transpiler, {
		exts: extensions,
		matcher,
	});
};
