import { html } from 'js-beautify';
import CacheControl from 'cache-with-resolver';

import { readonly } from './decorators/readonly';
import { isClassElement } from './helpers/predicate/isClassElement';
import { renderStringNode } from './helpers/render/renderStringNode';
import { withDuration } from './helpers/withDuration';
import View, { Context, FunctionComponent, NodeConstructor, Props, Component } from './View';
import ViewNode from './ViewNode';
import { SubscriptionManager } from './subscriptions/SubscriptionManager';
import { SubscriptionEnum } from './subscriptions/SubscriptionsEnum';
import { CacheRecordData } from './decorators/cache';
import { ViewEngine, ClassComponent } from './index';

export default class ViewBuilder {
	layout: FunctionComponent | ClassComponent;

	@readonly
	private readonly root: FunctionComponent | ClassComponent;
	@readonly
	private readonly viewEngine: ViewEngine;
	@readonly
	private readonly context: Context;
	@readonly
	private readonly cacheControl: CacheControl;
	@readonly
	private readonly viewStore: Record<string, any>;

	constructor(...args: [ViewEngine, CacheControl, ...any[]]) {
		// Init cascade
		if (args.length > 2 && args[0] instanceof ViewEngine) {
			this.viewEngine = args[0];
			this.cacheControl = args[1];
			if (args.length === 4) {
				this.root = args[2];
				this.context = args[3];
			} else if (args.length === 3) {
				if (typeof args[2] === 'function') {
					this.root = args[2];
				} else if (typeof args[2] === 'object') {
					this.context = args[2];
				}
			}
		} else {
			throw Error('Invalid initialization of ViewBuilder');
		}
		// Another logic
		this.layout = this.viewEngine.config.layout;
		this.viewStore = {};
	}

	//
	// RENDER TRIGGER
	//

	/**
	 * Render passed View from the root
	 * @returns Promise
	 */
	async toString(): Promise<string> {
		let output = this.viewEngine.config.logRenderTime ? await this._renderWithDuration() : await this._render();
		if (this.viewEngine.config.beautifyOutput) {
			output = this._beautifyHTML(output);
		}
		return output;
	}

	//
	// HELPERS
	//

	private async _renderWithDuration(): Promise<string> {
		let output = await withDuration(this._render.bind(this))();
		try {
			this.viewEngine.config.logger('Rendering took ' + output.duration / 1000 + 's');
		} catch (err) {
			throw new Error(err + '\n\nThe error above raised from logger passed in config.');
		}
		return output.output;
	}

	private _beautifyHTML(htmlString: string) {
		return html(htmlString, this.viewEngine.config.beautifyOptions);
	}

	private _mockNode(nodeConstructor: NodeConstructor, props: Props = {}, children: any[] = []) {
		return new ViewNode(nodeConstructor, props, children);
	}

	private _getCacheKey(node: ViewNode): [ClassComponent | string | number, number] {
		const _constructor = node.nodeConstructor as ClassComponent;
		const _data = SubscriptionManager.getSubscriptionData<CacheRecordData>(_constructor, SubscriptionEnum.CACHE);
		// When _data is false component is subscribed to cache and constructor is key
		// When data is object and has key-function resolve function to get key
		// When data is object nad has no key-function contructor is key
		// Always return tuple [key, ttl], ttl is null of non data object records
		if (_data === false) return [_constructor, null];
		else if (_data) {
			const _castedData = _data as CacheRecordData;
			if (_castedData.resolveKey)
				return [_castedData.resolveKey(node.props, this.context), _castedData.timeToLive];
			return [_constructor, _castedData.timeToLive];
		}
		return [null, null];
	}

	private _getViewStore(node: ViewNode): Record<string, any> {
		const _constructor = node.nodeConstructor as ClassComponent;
		if (SubscriptionManager.isSubscribeTo(_constructor, SubscriptionEnum.VIEW_STORE)) return this.viewStore;
		return undefined;
	}

	//
	// RENDERING CORE
	//

	private async _renderLayout(page: string): Promise<string> {
		const _mockedNode = this._mockNode(this.layout);
		let _layoutHtml = await this._resolveNodeOutput(_mockedNode);
		_layoutHtml = _layoutHtml.replace('##PLACEHODLER##', page);
		return _layoutHtml;
	}

	private async _render(): Promise<string> {
		const _mockedNode = this._mockNode(this.root);
		const _documentHtmlPromise = this._resolveNodeOutput(_mockedNode);

		if (this.layout) {
			return this._renderLayout(await _documentHtmlPromise);
		} else {
			return _documentHtmlPromise;
		}
	}

	/**
	 * This method takes care of transforming node into string
	 * It may call furhter methods declared below
	 * It hold whole lifecycle of Class component
	 */
	private async _renderNode(node: ViewNode): Promise<string> {
		// expose own children to the component
		node.props.children = node.children;

		// get inital information about caches
		const [_cacheKey, _cacheTTL] = this._getCacheKey(node);
		// Cached HTML will be looked up for not null _cacheKey if cacheControl exists
		const _cachcedHTML = _cacheKey && this.cacheControl && this.cacheControl.get(_cacheKey);
		// get viewStore
		const _viewStore = this._getViewStore(node);

		/**
		 * LIFECYCLE
		 */

		// Init class Component and save it to the node object
		if (isClassElement(node)) {
			// antipattern cast TODO: figure out better solution
			// eslint-disable-next-line
			node.componentInstance = new (node as any).nodeConstructor(
				node.props,
				this.context,
				_viewStore
			) as View.Component;
		}

		let _html = _cachcedHTML;
		// Run lifecycle if no cachce was found (it's never found for function components -> functions cant be decorated in current TS)
		if (!_html) {
			// if onInit declared run it and discared potential return
			if (isClassElement(node)) {
				node.componentInstance.onInit && void (await node.componentInstance.onInit());
			}

			/**
			 * Rendered indirect children are passed through props.children, thus indirect children needs to be rendered first
			 * Indirect children are directly between component tags (<component><IndirectChild /></component>)
			 * Childrne are present in props as Promise<strin[]>
			 */
			node.props.children = await this._renderIndirectChildren(node.props.children);

			/**
			 * If it's class component run onPrerender before rendering direct children
			 */
			if (isClassElement(node)) {
				// if onPrere
				node.componentInstance.onPreRender && void (await node.componentInstance.onPreRender());
			}

			/**
			 * Direct children are inside component and are resolved by executing the component
			 */
			_html = await this._renderDirectChildren(node);

			// store chache if _cahceKey is not null and cacheControl exists
			if (_cacheKey && this.cacheControl) {
				// delete just to be save
				this.cacheControl.delete(_cachcedHTML);
				this.cacheControl.set(_cacheKey, _html, _cacheTTL);
			}
		}

		// Class component can edit resulting HTML in it's method if implemented
		// HTML from cache is also subject to onRender
		// html is cached before onPreRender
		if (isClassElement(node)) {
			_html = node.componentInstance.onRender ? await node.componentInstance.onRender(_html) : _html;
		}

		return _html;
	}

	/**
	 * Indirect children are either ViewNode or anything else
	 */
	private _renderIndirectChildren(children: (ViewNode | string)[]) {
		return this._renderArray(children);
	}
	private _renderArray(children: (string | ViewNode)[]): Promise<string[]> {
		let childrenPromises = children.map(async child => {
			if (child instanceof ViewNode) {
				return this._renderNode(child);
			} else {
				return String(child);
			}
		});
		return Promise.all(childrenPromises);
	}

	/**
	 * Resolve direct children or render node, ultimately return string of rendered noed
	 * String in nodeConstructor means there are no direct children, thus allowing render to HTML
	 * Function in nodeConstructor means there are direct children
	 * Possible functions: ClassComponent, FunctionComponent, Fragment (special type of FunctionComponent)
	 */
	private async _renderDirectChildren(node: ViewNode): Promise<string> {
		if (typeof node.nodeConstructor === 'function') {
			/**
			 * Fragment returns (string|ViewNode)[] from props.children imidiately instead of a ViewNode
			 * Components can also return string (or anything else without because, will be casted as string) imidiately without returning any JSX
			 * Components can also return array of component without wrapping component
			 */
			let resolution: ViewNode | ViewNode[] | string[] | string = null;
			// Class components are already instanced, so continue resolving them as such
			if (node.componentInstance) {
				resolution = node.componentInstance.render() as ViewNode | string[] | string;
			} else {
				// FunctionComponent
				// antipatern cast TODO: figure out better solution
				resolution = (node as any).nodeConstructor(node.props, this.context) as ViewNode | string[] | string;
			}

			if (resolution) return this._resolveNodeOutput(resolution);
		} else if (typeof node.nodeConstructor === 'string') {
			// Render ViewNode with string nodeConstructor
			return renderStringNode(node);
		}

		return '';
	}

	/**
	 * Class render method of function returns something, this determines what it is and how to proceed
	 */
	private async _resolveNodeOutput(resolution: ViewNode | ViewNode[] | string[] | string) {
		if (resolution instanceof ViewNode) {
			/**
			 * Resolution created a new ViewNode that needs to be rednered
			 */
			return this._renderNode(resolution);
		} else if (Array.isArray(resolution)) {
			/**
			 * Resolution created a an array of ViewNodes or anys
			 */
			let arrayResolution = await this._renderArray(resolution);
			return arrayResolution.join('');
		}
		// In case component returned anything else, make it a string
		return String(resolution);
	}
}
