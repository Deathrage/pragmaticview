// 3rd packages
import CacheControl, { CacheControlConfig as CacheConfig } from 'cache-with-resolver';

// Core
import { IntrinsicElements as inEls } from './intrinsic/IntrinsicElements';
import View, { ClassComponent, Context, FunctionComponent, NodeConstructor, Props } from './View';
import ViewBuilder from './ViewBuilder';
import ViewNode from './ViewNode';

// Register
import { register, RegisterConfig } from './register';

// Decorators
import { cache } from './decorators/cache';
import { readonly } from './decorators/readonly';
import { viewStore } from './decorators/viewStore';

// Helpers
import { Helpers } from './Helpers';

type ineEls = inEls;
declare global {
	namespace JSX {
		type IntrinsicElements = ineEls;
		type Element = ViewNode | any;
		interface ElementClass extends View.Component {
			render(): JSX.Element;
		}
		// Common props for non elements
		interface IntrinsicAttributes {}
	}
}

export interface ViewEngineConfig {
	beautifyOutput?: boolean;
	beautifyOptions?: Object;
	logger?: (message: string) => void;
	logRenderTime?: boolean;
	singleton?: boolean;
	layout?: FunctionComponent | ClassComponent;
	cacheConfig?: CacheConfig;
}

const defaultConfig: ViewEngineConfig = {
	beautifyOutput: false,
	logger: console.log,
	logRenderTime: false,
	singleton: false,
};

let singleton = false;
(global as any).deSingleton = () => void (singleton = false);

class ViewEngine {
	@readonly
	public readonly config: ViewEngineConfig;
	@readonly
	public readonly cacheControl: CacheControl;

	/**
	 * @param  {ViewEngineConfig} config - Options of ViewEngien
	 */
	constructor(config?: ViewEngineConfig) {
		// Handle singleton
		if (singleton === true) {
			throw new Error(
				'First ViewEngine was set to singleton mode thus preventing another ViewEngine from initialization.'
			);
		}
		const defaultClone = Object.assign({}, defaultConfig);
		this.config = Object.assign(defaultClone, config || {});
		if (this.config.singleton === true) {
			singleton = true;
		}
		// Handle cache control
		this.cacheControl = this.config.cacheConfig && new CacheControl(this.config.cacheConfig);
	}
	/**
	 * Augment require for specific path and transpiles JSX factory during require process
	 * File cannot be transpiled by any other kind of transpiler
	 * @param  {string} pth
	 * @returns void
	 */
	static register(pth: string, config?: RegisterConfig): void {
		register(pth, config || {});
	}

	/**
	 * @returns ViewBuilder
	 */
	getBuilder(): ViewBuilder;
	/**
	 * @param  {FunctionComponent|ClassComponent} root - Root component of the View, usually html tag
	 * @returns ViewBuilder
	 */
	getBuilder(root: FunctionComponent | ClassComponent): ViewBuilder;
	/**
	 * @param  {Context} context - Context of the rendering that is available to all components
	 * @returns ViewBuilder
	 */
	getBuilder(context: Context): ViewBuilder;
	/**
	 * @param  {FunctionComponent|ClassComponent} root - Root component of the View, usually html tag
	 * @param  {Context} context - Context of the rendering that is available to all components
	 * @returns ViewBuilder
	 */
	getBuilder(root: FunctionComponent | ClassComponent, context: Context): ViewBuilder;
	getBuilder(...args: any[]): ViewBuilder {
		let vb = new ViewBuilder(this, this.cacheControl, ...args);
		return vb;
	}

	/**
	 * Directly renders component, handles viewBuilder lifecycle internaly
	 * @param  {FunctionComponent|ClassComponent} root
	 * @param  {Context} context
	 * @returns Promise
	 */
	render(root: FunctionComponent | ClassComponent, context?: Context): Promise<string> {
		return this.getBuilder(root, context).toString();
	}
}

// decorators wrap
const Decorators = {
	cache,
	readonly,
	viewStore,
};

// Export all necessary
export {
	/** Core */
	View,
	ViewEngine,
	Decorators,
	Helpers,
	/** Types */
	ViewBuilder,
	ViewNode,
	NodeConstructor,
	ClassComponent,
	FunctionComponent,
	Props,
	Context,
	CacheConfig,
};
