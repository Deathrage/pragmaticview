import { Predicate, predicate } from './helpers/predicate/predicate';
import { Elements, elements } from './helpers/elements/elements';

export interface Helpers {
	predicate: Predicate;
	elements: Elements;
}

export const Helpers: Helpers = {
	predicate,
	elements,
};
