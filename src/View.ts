import { createElement } from './helpers/elements/createElement';

export interface Props {
	[key: string]: any;
	children?: JSX.Element[];
}

export interface Context {}

export abstract class Component<P = Props, C = any, VS = any> {
	constructor(protected props?: P | Props, protected context?: C, protected viewStore?: VS) {}
	// lifecycle
	onInit?(): Promise<void> | void;
	onPreRender?(): Promise<void> | void;
	onRender?(html: string): Promise<string> | string;
	abstract render(): JSX.Element;
}

const View = (nodeConstructor: NodeConstructor, props: Props, ...children: JSX.Element[]): JSX.Element => {
	return createElement(nodeConstructor, props, children);
};

View.Component = Component;
View.Fragment = (props?: Props) => {
	return props.children;
};
View.Placeholder = (props: {}) => {
	return `##PLACEHODLER##`;
};

export type ClassComponent<P = Props, C = Context> = new () => Component<P, C>;
export type FunctionComponent = (props?: Props, context?: Context) => JSX.Element;
export type NodeConstructor = string | FunctionComponent | ClassComponent;

export default View;
