import { NodeConstructor, Props, Component } from './View';

export type ViewNodeChildren = JSX.Element[];

export default class ViewNode<NC extends NodeConstructor = any> {
	public componentInstance: Component;
	constructor(public nodeConstructor: NC, public props: Props, public children: ViewNodeChildren) {}
}
