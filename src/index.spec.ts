import { expect } from 'chai';
import { Helpers } from './Helpers';
import { ViewEngine, ViewEngineConfig } from './index';
const clearModule = require('clear-module');

describe('Test viewEngine', () => {
	it('Singleton viewEngine', () => {
		let config: ViewEngineConfig = {
			singleton: true,
		};
		let vE = new ViewEngine(config);
		expect(() => new ViewEngine()).to.throw();
	});

	it('Should imidiately render', async () => {
		const Component = () => Helpers.elements.createElement('div', {}, ['foo']);
		let vE = new ViewEngine();
		let result = await vE.render(Component);
		expect(result).to.equal('<div>foo</div>');
	});

	afterEach(() => {
		clearModule.all();
		(global as any).deSingleton && (global as any).deSingleton();
	});
});
