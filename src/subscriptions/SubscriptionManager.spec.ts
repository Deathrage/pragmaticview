import { expect } from 'chai';
import { Component } from '../View';
import { SubscriptionManager, SubscriptionObject } from './SubscriptionManager';

describe('Subscription manager', () => {
	it('Should subscribe component to foo', () => {
		class Comp extends Component {
			render() {
				throw new Error('Method not implemented.');
			}
		}

		SubscriptionManager.subscribe(Comp, 'foo' as any);
		expect(SubscriptionManager.isSubscribeTo(Comp, 'foo' as any)).to.equal(true);
	});

	it('Should list all subscirptions of component', () => {
		class CompUs extends Component {
			render() {
				throw new Error('Method not implemented.');
			}
		}

		SubscriptionManager.subscribe(CompUs, 'foo' as any);
		SubscriptionManager.subscribe(CompUs, 'bar' as any);
		expect(SubscriptionManager.getSubscriptions(CompUs)).to.eql(['foo', 'bar']);
	});

	it('Should unsubscribe component from single one -> remains', () => {
		class CompA extends Component {
			render() {
				throw new Error('Method not implemented.');
			}
		}

		SubscriptionManager.subscribe(CompA, 'foo' as any);
		SubscriptionManager.subscribe(CompA, 'bar' as any);
		SubscriptionManager.unsubscribe(CompA, 'foo' as any);
		expect(SubscriptionManager.isSubscribeTo(CompA, 'bar' as any)).to.equal(true);
	});

	it('Should unsubscribe component from all', () => {
		class CompB extends Component {
			render() {
				throw new Error('Method not implemented.');
			}
		}

		SubscriptionManager.subscribe(CompB, 'foo' as any);
		SubscriptionManager.unsubscribe(CompB, 'foo' as any);
		expect(SubscriptionManager.getSubscriptions(CompB)).to.eql([]);
	});

	it('Should subscribe with data', () => {
		class CompC extends Component {
			render() {
				throw new Error('Method not implemented.');
			}
		}

		SubscriptionManager.subscribe(CompC, 'foo' as any, { d: 5 });
		SubscriptionManager.subscribe(CompC, 'bar' as any);
		const data = SubscriptionManager.getSubscriptions(CompC).find(
			(x: SubscriptionObject) => x.type === ('foo' as any)
		);
		expect(data).to.eql({ type: 'foo', data: { d: 5 } });
	});

	it('Should reutrn false as no object is present', () => {
		class CompD extends Component {
			render() {
				throw new Error('Method not implemented.');
			}
		}

		SubscriptionManager.subscribe(CompD, 'bar' as any);
		const data = SubscriptionManager.getSubscriptionData(CompD, 'bar' as any);
		expect(data).to.equal(false);
	});

	it('Should return data from object', () => {
		class CompE extends Component {
			render() {
				throw new Error('Method not implemented.');
			}
		}

		SubscriptionManager.subscribe(CompE, 'foo' as any);
		SubscriptionManager.subscribe(CompE, 'bar' as any, { d: 5 });
		const data = SubscriptionManager.getSubscriptionData(CompE, 'bar' as any);
		expect(data).to.eql({ d: 5 });
	});

	it('Should return null as no record exist', () => {
		class CompF extends Component {
			render() {
				throw new Error('Method not implemented.');
			}
		}
		const data = SubscriptionManager.getSubscriptionData(CompF, 'bar' as any);
		expect(data).to.eql(null);
	});
});
