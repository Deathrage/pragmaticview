import { ClassComponent, FunctionComponent } from '../View';
import { SubscriptionEnum } from './SubscriptionsEnum';

type NonNativeComponent = FunctionComponent | ClassComponent;

export interface SubscriptionObject {
	type: SubscriptionEnum;
	data: any;
}
type SubscriptionRecord = SubscriptionEnum | SubscriptionObject;

const subscriptions = new Map<NonNativeComponent, SubscriptionRecord[]>();

export namespace SubscriptionManager {
	const getIndex = (record: SubscriptionRecord[], name: SubscriptionEnum) =>
		record.findIndex(x => x === name || (x as SubscriptionObject).type === name);

	export const subscribe = <D = any>(
		component: NonNativeComponent,
		subscriptionType: SubscriptionEnum,
		data: D = null
	) => {
		// plcaeholder array
		let record = [];
		if (subscriptions.has(component)) {
			// if has record, get it check if this type is present
			// if present don to anything, otherwise delete record and add new with the typer
			record = subscriptions.get(component);
			if (getIndex(record, subscriptionType) > -1) return;
			subscriptions.delete(component);
		}

		const _newRecord: SubscriptionRecord = data ? { type: subscriptionType, data } : subscriptionType;

		return void subscriptions.set(component, [...record, _newRecord]);
	};

	export const unsubscribe = (component: NonNativeComponent, subscriptionType: SubscriptionEnum = null) => {
		// only if is present
		if (subscriptions.has(component)) {
			// Get record and dlete it, if type is specified return record back without specified type
			const record = subscriptions.get(component);
			void subscriptions.delete(component);
			if (subscriptionType) {
				void record.splice(getIndex(record, subscriptionType), 1);
				return void subscriptions.set(component, record);
			}
		}
	};

	export const isSubscribeTo = (component: NonNativeComponent, subscriptionType: SubscriptionEnum) => {
		if (subscriptions.has(component)) {
			const record = subscriptions.get(component);
			return getIndex(record, subscriptionType) > -1;
		}
		return false;
	};

	export const getSubscriptions = (component: NonNativeComponent) => {
		if (subscriptions.has(component)) {
			return subscriptions.get(component);
		}
		return [];
	};

	/**
	 * If no record found returns null, if record found but it's not object (with data) return false
	 * @param  {NonNativeComponent} nodeConstructor
	 * @param  {SubscriptionEnum} subscriptionType
	 */
	export const getSubscriptionData = <D = any>(
		component: NonNativeComponent,
		subscriptionType: SubscriptionEnum
	): D | boolean => {
		if (subscriptions.has(component)) {
			const recordObject: SubscriptionObject = subscriptions
				.get(component)
				.find((x: SubscriptionObject) => x.type === subscriptionType) as SubscriptionObject;
			if (!recordObject) return false;
			return recordObject.data;
		}
		return null;
	};
}
