import ViewNode from '../../ViewNode';

/**
 * Validates if object is valid element (either instance of ViewNode or duck-typed ViewNode)
 * @param  {JSX.Element} element
 */
export const isElement = (obj: any) => {
	if (typeof obj === 'object') {
		if (obj instanceof ViewNode) return true;
		if (obj.nodeConstructor) return true;
	}
	return false;
};
