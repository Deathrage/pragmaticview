import { expect } from 'chai';

import ViewNode from '../../ViewNode';
import View from '../../View';
import { isNativeElement } from './isNativeElement';

class ClassComponent extends View.Component {
	render() {
		throw new Error('Method not implemented.');
	}
	constructor(...args) {
		super(...args);
	}
}

const FunctionComponent = () => 'foo';

describe('Check if node is a native element -> direct HTML', () => {
	it('should determine it is a string', () => {
		const fakeNode = new ViewNode('div', {}, []);
		expect(isNativeElement(fakeNode)).to.equal(true);
	});
	it("should determine it is not a string as it's class", () => {
		const fakeNode = new ViewNode(ClassComponent, {}, []);
		expect(isNativeElement(fakeNode)).to.equal(false);
	});
	it("should determine it is not a string as it's function", () => {
		const fakeNode = new ViewNode(FunctionComponent, {}, []);
		expect(isNativeElement(fakeNode)).to.equal(false);
	});
});
