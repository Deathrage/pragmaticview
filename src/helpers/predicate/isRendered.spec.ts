import { expect } from 'chai';

import ViewNode from '../../ViewNode';
import { isRendered } from './isRendered';

const fakeNode = new ViewNode('div', {}, []);
const renderedNode = '<div></div>';

describe('Check if node is a string component', () => {
	it('should determine it is rendered', () => {
		expect(isRendered(renderedNode)).to.equal(true);
	});
	it('should determine it is still a node', () => {
		expect(isRendered(fakeNode)).to.equal(false);
	});
});
