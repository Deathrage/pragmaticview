import ViewNode from '../../ViewNode';
import View from '../../View';
import { isClass } from './isClass';
import { isElement } from './isElement';

export const isFunctionElement = (obj: JSX.Element) => {
	if (isElement(obj)) {
		const _node: ViewNode = obj;
		if (typeof _node.nodeConstructor === 'function') {
			if (!View.Component.isPrototypeOf(_node.nodeConstructor) && !isClass(_node.nodeConstructor)) {
				return true;
			}
		}
	}
	return false;
};
