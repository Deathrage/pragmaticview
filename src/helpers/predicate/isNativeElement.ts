import ViewNode from '../../ViewNode';
import { isElement } from './isElement';

export const isNativeElement = (obj: JSX.Element) => {
	if (isElement(obj)) {
		const _node: ViewNode = obj;
		if (typeof _node.nodeConstructor === 'string') {
			return true;
		}
	}
	return false;
};
