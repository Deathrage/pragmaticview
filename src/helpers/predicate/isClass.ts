/**
 * Checks if function is contructor of class
 * @param  {Function} func
 */
export const isClass = (func: Function) => typeof func === 'function' && /^class\s/.test(Function.prototype.toString.call(func));
