import { expect } from 'chai';

import ViewNode from '../../ViewNode';
import View from '../../View';
import { isClassElement } from './isClassElement';

class ClassComponent extends View.Component {
	render() {
		throw new Error('Method not implemented.');
	}
	constructor(...args) {
		super(...args);
	}
}

const FunctionComponent = () => 'foo';

describe('Check if node is a class element', () => {
	it('should determine it is a class', () => {
		const fakeNode = new ViewNode(ClassComponent, {}, []);
		expect(isClassElement(fakeNode)).to.equal(true);
	});
	it("should determine it is not a class as it's function", () => {
		const fakeNode = new ViewNode(FunctionComponent, {}, []);
		expect(isClassElement(fakeNode)).to.equal(false);
	});
	it("should determine it is not a class as it's string", () => {
		const fakeNode = new ViewNode('img', {}, []);
		expect(isClassElement(fakeNode)).to.equal(false);
	});
});
