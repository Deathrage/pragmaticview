import { expect } from 'chai';
import { isClass } from './isClass';

describe('IsClass helper', () => {
	it('Should recognize function', () => {
		function func() {};
		expect(isClass(func)).to.equal(false);
	});
	it('Should recognize arrow function', () => {
		const func = () => {};
		expect(isClass(func)).to.equal(false);
	});
	it('Should recognize class', () => {
		class A {}
		expect(isClass(A)).to.equal(true);
	});
});
