import { expect } from 'chai';

import ViewNode from '../../ViewNode';
import View from '../../View';
import { isElement } from './isElement';

class ClassComponent extends View.Component {
	render() {
		throw new Error('Method not implemented.');
	}
	constructor(...args) {
		super(...args);
	}
}

const FunctionComponent = () => 'foo';

describe('Check if node is an element', () => {
	it('should determine it is instance of viewnode', () => {
		const fakeNode = new ViewNode(ClassComponent, {}, []);
		expect(isElement(fakeNode)).to.equal(true);
	});
	it('should determine it is duck-typed viewnode', () => {
		const fakeNode = {
			nodeConstructor: FunctionComponent,
		};
		expect(isElement(fakeNode)).to.equal(true);
	});
	it('should determine it is not an element', () => {
		expect(isElement('foo')).to.equal(false);
	});
});
