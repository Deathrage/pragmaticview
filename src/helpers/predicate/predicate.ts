import { isClassElement } from './isClassElement';
import { isFunctionElement } from './isFunctionElement';
import { isNativeElement } from './isNativeElement';
import { isElement } from './isElement';
import { isRendered } from './isRendered';

export type PredicateFunction = (obj: JSX.Element) => boolean;
export interface Predicate {
	isElement: PredicateFunction;
	isClassElement: PredicateFunction;
	isFunctionElement: PredicateFunction;
	isNativeElement: PredicateFunction;
	isRendered: PredicateFunction;
}

export const predicate: Predicate = {
	isElement,
	isClassElement,
	isFunctionElement,
	isNativeElement,
	isRendered,
};
