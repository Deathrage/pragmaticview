import { expect } from 'chai';

import ViewNode from '../../ViewNode';
import View from '../../View';
import { isFunctionElement } from './isFunctionElement';

class ClassComponent extends View.Component {
	render() {
		throw new Error('Method not implemented.');
	}
	constructor(...args) {
		super(...args);
	}
}

const FunctionComponent = () => 'foo';

describe('Check if node is a function component', () => {
	it('should determine it is a function', () => {
		const fakeNode = new ViewNode(FunctionComponent, {}, []);
		expect(isFunctionElement(fakeNode)).to.equal(true);
	});
	it("should determine it is not a function as it's class", () => {
		const fakeNode = new ViewNode(ClassComponent, {}, []);
		expect(isFunctionElement(fakeNode)).to.equal(false);
	});
	it("should determine it is not a function as it's string", () => {
		const fakeNode = new ViewNode('img', {}, []);
		expect(isFunctionElement(fakeNode)).to.equal(false);
	});
});
