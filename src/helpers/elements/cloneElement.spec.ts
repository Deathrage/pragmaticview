import { expect } from 'chai';
import ViewNode from '../../ViewNode';
import { cloneElement } from './cloneElement';

const FakeComponent = () => 'foo';

describe('Clone element helper', () => {
	it('Should clone viewnode', () => {
		const _node = new ViewNode(
			FakeComponent,
			{
				foo: 'bar',
			},
			[]
		);
		expect(cloneElement(_node)).to.eql(_node);
	});
	it("Cloned viewnode's props should not be mutated", () => {
		const _node = new ViewNode(
			FakeComponent,
			{
				foo: 'bar',
			},
			[]
		);
		const _cloneNode = cloneElement(_node);
		_cloneNode.props.mango = 'apple';
		expect(_node.props).not.to.eql(_cloneNode.props);
	});
	it("Clone viewnpde's children should not be mutated", () => {
		const _node = new ViewNode(FakeComponent, {}, ['child']);
		const _cloneNode = cloneElement(_node);
		_cloneNode.children.push('foo');
		expect(_node.children).not.to.eql(_cloneNode.children);
	});
});
