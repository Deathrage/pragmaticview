import { expect } from 'chai';
import ViewNode from '../../ViewNode';
import { createElement } from './createElement';

const FakeComponent = () => 'foo';

describe('Create element helper', () => {
	it('Should create element', () => {
		const _node = createElement(FakeComponent, {}, []);
		expect(_node instanceof ViewNode).to.equal(true);
	});
});
