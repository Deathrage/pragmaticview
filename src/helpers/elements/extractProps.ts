import ViewNode from '../../ViewNode';
import { isElement } from '../predicate/isElement';
import cloneDeep = require('clone-deep');

/**
 * Returns cloned props of the component.
 * @param  {JSX.Element} element
 * @returns Props
 */
export const extractProps = <P = {}>(element: JSX.Element): P => {
	if (isElement(element)) {
		const _viewNode: ViewNode<any> = element;
		return cloneDeep(_viewNode.props as P);
	}
	return null;
};
