import * as flatten from 'array-flatten';
import ViewNode from '../../ViewNode';
import { NodeConstructor, Props } from '../../View';

/**
 * Assembles virtual DOM node, children array is flattened
 * @param  {NodeConstructor} constructor
 * @param  {Props} props
 * @param  {JSX.Element[]} children
 */
export const createElement = <NC extends NodeConstructor>(constructor: NC, props: Props, children: JSX.Element[]) =>
	new ViewNode(constructor, props || {}, flatten(children || []));
