import { NodeConstructor, Props } from '../../View';
import ViewNode from '../../ViewNode';
import { createElement } from './createElement';
import { cloneElement } from './cloneElement';
import { extractProps } from './extractProps';
import { injectProps } from './injectProps';

export interface Elements {
	createElement: <NC extends NodeConstructor>(
		constructor: NC,
		props: Props,
		children?: JSX.Element[]
	) => ViewNode<NC>;
	cloneElement: <E = JSX.Element>(element: E) => E;
	extractProps: <P = {}>(element: JSX.Element) => P;
	injectProps: <E = JSX.Element, NP = Props>(element: E, props: NP) => E;
}

export const elements: Elements = {
	createElement,
	cloneElement,
	extractProps,
	injectProps,
};
