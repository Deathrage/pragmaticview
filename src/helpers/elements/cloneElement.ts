import { isRendered } from '../predicate/isRendered';
import { isClassElement } from '../predicate/isClassElement';
import { isFunctionElement } from '../predicate/isFunctionElement';
import { isNativeElement } from '../predicate/isNativeElement';
import ViewNode from '../../ViewNode';
import cloneDeep = require('clone-deep');

/**
 * Clones element, props and children are cloned too to prevent mutation.
 * @param  {JSX.Element} element
 * @returns JSX
 */
export const cloneElement = <E>(element: E): E => {
	if (isRendered(element)) return element;
	if (isNativeElement(element) || isFunctionElement(element) || isClassElement(element)) {
		const _viewNode: ViewNode<any> = element as any;
		const [_clonedProps, _clonedChildren] = [cloneDeep(_viewNode.props), (_viewNode.children || []).slice(0)];
		return (new ViewNode(_viewNode.nodeConstructor, _clonedProps, _clonedChildren) as any) as E;
	}
	return null;
};
