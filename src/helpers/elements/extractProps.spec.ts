import { expect } from 'chai';
import { createElement } from './createElement';
import { extractProps } from './extractProps';

const FakeComponent = () => 'foo';

describe('Extract props helper', () => {
	it('Should extract props', () => {
		const _node = createElement(FakeComponent, {
			foo: 'bar',
		}, []);
		expect(extractProps(_node)).to.eql({
			foo: 'bar',
		});
	});
	it('Should return null when called on non-element', () => {
		const _notElement = 'foo';
		expect(extractProps(_notElement)).to.equal(null);
	});
});
