import { expect } from 'chai';
import { createElement } from './createElement';
import { extractProps } from './extractProps';
import { injectProps } from './injectProps';

const FakeComponent = () => 'foo';

describe('Inject props helper', () => {
	it('Should inject props to element', () => {
		const _node = createElement(
			FakeComponent,
			{
				foo: 'bar',
			},
			[]
		);
		injectProps(_node, {
			fruit: 'peach',
		});
		expect(extractProps(_node)).to.eql({
			foo: 'bar',
			fruit: 'peach',
		});
	});
	it('Should return null when called on non-element', () => {
		const _notElement = 'foo';
		expect(injectProps(_notElement, { foo: 'bar' })).to.equal(null);
	});
	it('Injected props should be immutable', () => {
		const _props: Record<string, string> = {
			foo: 'bar',
		};
		const _node = createElement(FakeComponent, {}, []);
		injectProps(_node, _props);
		_props.fruit = 'mango';
		expect(extractProps(_node)).to.eql({
			foo: 'bar',
		});
	});
});
