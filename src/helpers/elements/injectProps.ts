import { Props } from '../../View';
import ViewNode from '../../ViewNode';
import { isNativeElement } from '../predicate/isNativeElement';
import { isFunctionElement } from '../predicate/isFunctionElement';
import { isClassElement } from '../predicate/isClassElement';
import cloneDeep = require('clone-deep');

/**
 * Inject copy of passed props (prevent mutation) to the component, old and new props are spreaded in new object
 * @param  {JSX.Element} element
 * @param  {Props} props
 * @returns void
 */
export const injectProps = <E = JSX.Element, NP = Props>(element: E, props: NP): E => {
	if (isNativeElement(element) || isFunctionElement(element) || isClassElement(element)) {
		const _viewNode: ViewNode<any> = element as any;
		_viewNode.props = {
			..._viewNode.props,
			...cloneDeep(props),
		};
		return (_viewNode as any) as E;
	}
	return null;
};
