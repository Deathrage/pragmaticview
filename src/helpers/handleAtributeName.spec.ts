import { expect } from 'chai';
import { handleAtributeName } from './handleAtributeName';

describe('Transform JS HTMLElement properties to HTML Attributes', () => {
	it('Transform className to class', () => {
		expect(handleAtributeName('className')).to.equal('class');
	});
	it('Transform httpEquiv', () => {
		expect(handleAtributeName('httpEquiv')).to.equal('http-equiv');
	});
	it('Any other attribute', () => {
		expect(handleAtributeName('rowSpan')).to.equal('rowspan');
	});
});
