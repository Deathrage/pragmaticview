export function handleAtributeName(item: string) {
	let lowerVariant = item.toLowerCase();
	if (lowerVariant === 'classname') lowerVariant = 'class';
	if (lowerVariant === 'httpequiv') lowerVariant = 'http-equiv';
	return lowerVariant;
}
