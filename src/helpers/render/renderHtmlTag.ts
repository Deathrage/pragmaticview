export const renderSelfClosingHTMLTag = (tagName: string, propertyString: string) => {
	return `<${tagName}${propertyString ? ' ' + propertyString : ''}/>`;
};

export const renderHTLTagWithChildren = (tagName: string, propertyString: string, innerHTML: string) => {
	return `<${tagName}${propertyString ? ' ' + propertyString : ''}>${innerHTML}</${tagName}>`;
};
