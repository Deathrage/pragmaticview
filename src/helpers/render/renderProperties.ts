import { string as toStyleString } from 'to-style';
import { Props } from '../../View';
import { handleAtributeName } from '../handleAtributeName';

export const transformStyleObject = (style: Object): string => toStyleString(style).replace('css-float', 'float');

export const renderProperty = (key: string, collection: any) => {
	let _specialProperty = null;
	if (key.toLowerCase() === 'style' && typeof collection[key] === 'object')
		_specialProperty = transformStyleObject(collection[key]);
	// if property exists and is bool true return just its name, otherwise render it as equal string or nothing
	return collection[key]
		? typeof collection[key] === 'boolean'
			? handleAtributeName(key)
			: `${handleAtributeName(key)}="${_specialProperty || String(collection[key])}"`
		: '';
};

export const renderProperties = (props: Props) => {
	let properties = Object.keys(props)
		.map(prop => renderProperty(prop, props))
		.join(' ');
	return properties;
};
