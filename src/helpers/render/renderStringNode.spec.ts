import { expect } from 'chai';
import ViewNode from '../../ViewNode';
import { renderStringNode } from './renderStringNode';

describe('Render string node', () => {
	it('Should render img string node', () => {
		const node: ViewNode = new ViewNode('img', { className: 'foo' }, []);
		expect(renderStringNode(node)).to.equal('<img class="foo"/>');
	});
	it('Should render div string node', () => {
		const node: ViewNode = new ViewNode(
			'div',
			{ className: 'foo', onClick: 'doMagic()', children: ['<img src="bar"/>'] },
			[]
		);
		expect(renderStringNode(node)).to.equal('<div class="foo" onclick="doMagic()"><img src="bar"/></div>');
	});
});
