import { expect } from 'chai';
import { renderSelfClosingHTMLTag, renderHTLTagWithChildren } from './renderHtmlTag';

describe('Render HTML tag helper', () => {
	it('Should render self closing tag', () => {
		expect(renderSelfClosingHTMLTag('img', 'class="foo"')).to.equal('<img class="foo"/>');
	});
	it('Should render tag with children', () => {
		expect(renderHTLTagWithChildren('div', 'class="foo"', '<img />')).to.equal('<div class="foo"><img /></div>');
	});
});
