import ViewNode from '../../ViewNode';
import { renderProperties } from './renderProperties';
import { renderSelfClosingHTMLTag, renderHTLTagWithChildren } from './renderHtmlTag';

const selfClosingTags = [
	'area',
	'base',
	'br',
	'col',
	'embed',
	'hr',
	'img',
	'input',
	'link',
	'meta',
	'param',
	'track',
	'wbr',
	'command',
	'keygen',
	'menuitem',
];

export const renderStringNode = (node: ViewNode) => {
	// Extract
	let tagName = node.nodeConstructor as string;
	let props = node.props || {};
	let children = props.children as string[];
	delete props.children;
	// Render parts
	let properties = renderProperties(props);
	let innerHtml = children && children.join('');
	// decide type of tag
	return selfClosingTags.includes(tagName.toLowerCase())
		? renderSelfClosingHTMLTag(tagName, properties)
		: renderHTLTagWithChildren(tagName, properties, innerHtml);
};
