import { expect } from 'chai';
import { renderProperty, renderProperties, transformStyleObject } from './renderProperties';

const collection = {
	className: 'foo',
	httpEquiv: 'bar',
	'data-test': 'mango',
	src: 'baguvix',
};

describe('Render properties helper', () => {
	it('Should render single property', () => {
		expect(renderProperty('className', collection)).to.equal('class="foo"');
	});
	it('Should render properties', () => {
		expect(renderProperties(collection)).to.equal('class="foo" http-equiv="bar" data-test="mango" src="baguvix"');
	});
	it('Should transform style object', () => {
		const style: Partial<CSSStyleDeclaration> = {
			marginBottom: '5px',
			cssFloat: 'right',
			justifyContent: 'flex-end',
			pointerEvents: 'auto',
		};
		expect(transformStyleObject(style)).to.equal(
			'margin-bottom: 5px; float: right; justify-content: flex-end; pointer-events: auto'
		);
	});
	it('Should transform style object inside attribute collection', () => {
		const style: Partial<CSSStyleDeclaration> = {
			marginBottom: '5px',
			cssFloat: 'right',
			justifyContent: 'flex-end',
			pointerEvents: 'auto',
		};
		const _collectionCopy = Object.assign({ style }, collection);
		expect(renderProperties(_collectionCopy)).to.equal(
			'style="margin-bottom: 5px; float: right; justify-content: flex-end; pointer-events: auto" class="foo" http-equiv="bar" data-test="mango" src="baguvix"'
		);
	});
});
