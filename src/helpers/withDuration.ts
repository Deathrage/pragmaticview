export const withDuration = (func: Function) => {
	return async (...args: any) => {
		let renderStart = Date.now();
		let output = await func(...args);
		return {
			output,
			duration: Date.now() - renderStart,
		};
	};
};
