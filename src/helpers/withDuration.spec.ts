import { expect } from 'chai';
import { withDuration } from './withDuration';

describe('With duration HOF', () => {
	it('Create func with stopwatch', async () => {
		let func = () =>
			new Promise(resolve => {
				setTimeout(resolve, 200);
			});
		let funcWithDur = withDuration(func);
		let result = await funcWithDur();
		expect(result.duration).to.be.lessThan(300);
	});
});
