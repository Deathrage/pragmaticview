import { expect } from 'chai';
import { View } from '.';

describe('Test View relatead features', () => {
	it('Create ViewNode', () => {
		let node = View('img', {
			src: 'test',
		});
		let testNode = {
			nodeConstructor: 'img',
			props: {
				src: 'test',
			},
			children: [] as any[],
		};
		expect(node).to.deep.equal(testNode);
	});

	it('Test fragment', () => {
		let renderedNode = ['<div></div>'];
		let fragResult = View.Fragment({ children: renderedNode });
		expect(fragResult).to.equal(renderedNode);
	});
});
