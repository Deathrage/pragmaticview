import { expect } from 'chai';
import { ViewBuilder } from '.';

describe('Test viewBuilder related', () => {
	it('ViewBuilder cant be invoedk directly', () => {
		expect(() => new ViewBuilder(null, null)).to.throw();
	});
});
