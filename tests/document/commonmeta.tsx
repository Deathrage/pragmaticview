/* eslint-disable */
import { View } from "../../src/View"

export const CommonMeta = (props: {title: string, children?: any}) => {
    return [
        <meta httpEquiv="Content-Type" content="text/html; charset=utf-8" />,
        <meta name="viewport" content="width=device-width, initial-scale=1"/>,
        <title>{props.title}</title>,
        props.children
    ];
}