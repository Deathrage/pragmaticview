/* eslint-disable */
import { View } from '../../src/View';
import { CommonMeta } from './commonmeta';
import { SampleTable } from './table';
import { SomeScripts } from './somescripts';
import BunnyVideo from './BunnyVideo';
import ContextReveal from '../components/ContextReveal'

export default class Document extends View.Component {
	render() {
		return (
			<html>
				<head>
					<CommonMeta title="This is test page">
						<link rel="stylesheet" type="text/css" href="theme.css" />
						<style type="text/css">
							{`table.fruitName td {
								font-size:11px;
								font-weight:bold;
								border:1px solid #ccc;
								padding:0 4px;
								background:#ccc;
								text-align:center;
							}`}
						</style>
					</CommonMeta>
				</head>
				<body>
					<ContextReveal />
					<a href="https://www.w3schools.com/html/tryit.asp?filename=tryhtml_styles_color" download>Download a page!</a>
					<BunnyVideo>
						<p>
							Video courtesy of 
							<a href="https://www.bigbuckbunny.org/" target="_blank">Big Buck Bunny</a>.
						</p>
					</BunnyVideo>
					<SampleTable />
					<SomeScripts />
				</body>
			</html>
		);
	}
}