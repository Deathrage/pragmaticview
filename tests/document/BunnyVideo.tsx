/* eslint-disable */
import { View } from "../../src/View";

export default class BunnyVideo extends View.Component {
    render() {
        return (
            <div className="video-wrap">
                <video width="400" controls>
                    <source src="mov_bbb.mp4" type="video/mp4" />
                    <source src="mov_bbb.ogg" type="video/ogg" />
                    Your browser does not support HTML5 video.
                </video>
                {this.props.children}
            </div>
        );
    }
}