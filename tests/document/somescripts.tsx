/* eslint-disable */
import { View } from "../../src/View"

export const SomeScripts = () => {
    return <script type="text/javascript"> {`

    Uize.require (
      [
        'UizeSite.Page.Example.library',
        'UizeSite.Page.Example',
        'Uize.Widget.TableSort'
      ],
      function () {
        'use strict';
    
        /*** create the example page widget ***/
          var page = window.page = UizeSite.Page.Example ();
    
        /*** spawn (and automatically wire) the Uize.Widget.TableSort object ***/
          Uize.Widget.TableSort.spawn (
            {
              idPrefix:'tableOfFruits',
              headingOverClass:'headingOver',
              headingLitClass:'headingLit',
              rowOverClass:'rowOver'
            },
            page
          );
    
        /*** wire up the page widget ***/
          page.wireUi ();
      }
    ); `}
    </script>
}