import { expect } from 'chai';

import { View, Helpers, ViewEngine } from '../../src';

let viewEngine: ViewEngine = null;
describe('It should test element helpers', () => {
	beforeEach(() => {
		viewEngine = new ViewEngine();
	});

	it('Should create elements', async () => {
		const Func = ({ title }) => <div>{title}</div>;
		class CompA extends View.Component {
			onInit() {
				this.props.children.push(Helpers.elements.createElement(Func, { title: 'Mango' }));
			}
			render() {
				return <span>{this.props.children}</span>;
			}
		}
		expect(await viewEngine.getBuilder(CompA).toString()).to.equal('<span><div>Mango</div></span>');
	});

	it('Should clone element and prevent mtuation', async () => {
		const FuncA = ({ apple }) => <div>{apple}</div>;
		class ComB extends View.Component {
			onInit() {
				this.props.children.push(Helpers.elements.cloneElement(this.props.children[0]));
				this.props.children[1].props.apple = 'orange';
			}
			render() {
				return this.props.children;
			}
		}
		const Wrap = () => (
			<ComB>
				<FuncA apple="banana" />
			</ComB>
		);
		expect(await viewEngine.getBuilder(Wrap).toString()).to.equal('<div>banana</div><div>orange</div>');
	});

	it('Should extract props from element', async () => {
		let extractedProps;
		const FuncA = ({ hotel, apple }) => <div />;
		class ComB extends View.Component {
			onInit() {
				extractedProps = Helpers.elements.extractProps(this.props.children[0]);
			}
			render() {
				return this.props.children;
			}
		}
		const Wrap = () => (
			<ComB>
				<FuncA hotel="trivago" apple="banana" />
			</ComB>
		);
		await viewEngine.getBuilder(Wrap).toString();
		expect(extractedProps).to.eql({
			hotel: 'trivago',
			apple: 'banana',
		});
	});

	it('Should inject props to element', async () => {
		const FuncA = ({ hotel = '' }) => <div>{hotel}</div>;
		class ComB extends View.Component {
			onInit() {
				Helpers.elements.injectProps(this.props.children[0], { hotel: 'trivago' });
			}
			render() {
				return this.props.children;
			}
		}
		const Wrap = () => (
			<ComB>
				<FuncA />
			</ComB>
		);
		expect(await viewEngine.getBuilder(Wrap).toString()).to.equal('<div>trivago</div>');
	});
});
