import { expect } from 'chai';

import { View, Helpers, ViewEngine } from '../../src';

let viewEngine: ViewEngine = null;
describe('It should test predicate helpers', () => {
	beforeEach(() => {
		viewEngine = new ViewEngine();
	});

	it('Should filter out non elements', async () => {
		const Func = () => <span>Func</span>;
		class ComA extends View.Component {
			onInit() {
				this.props.children = this.props.children.filter(Helpers.predicate.isElement);
			}
			render() {
				return <div>{this.props.children}</div>;
			}
		}
		const Wrap = () => (
			<ComA>
				<Func />
				Not element
				<Func />
				<Func />
				{5125}
				Something
				{{ foo: 'bar' }}
			</ComA>
		);
		expect(await viewEngine.getBuilder(Wrap).toString()).to.equal(
			'<div><span>Func</span><span>Func</span><span>Func</span></div>'
		);
	});
});
