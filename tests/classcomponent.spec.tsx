import { expect } from 'chai';
import { View, ViewEngine } from '../src/index';
import { DatabaseMock } from './mocks/DatabaseMock';

class ClassComponent extends View.Component {
	render() {
		return <div>{this.props ? this.props.children : ''}</div>;
	}
}

class WrapClassComponent extends View.Component {
	render() {
		return (
			<div>
				<ClassComponent>
					<ClassComponent />
				</ClassComponent>
				<article>
					<ClassComponent />
				</article>
			</div>
		);
	}
}

class LifeCycle extends View.Component {
	private value: string;
	async onInit() {
		this.value = 'test';
	}
	render() {
		return <div>{this.value}</div>;
	}
	async onRender(html: string) {
		return html.replace('test', 'replacedtest');
	}
}
class MockDatabaseCalls extends View.Component {
	private data: string;
	async onInit() {
		this.data = await DatabaseMock.callMock(500, 'database data');
	}
	render() {
		return <span>{this.data}</span>;
	}
}

class ConcurentCalls extends View.Component {
	render() {
		return (
			<div>
				<MockDatabaseCalls />
				<MockDatabaseCalls />
				<MockDatabaseCalls />
				<MockDatabaseCalls />
			</div>
		);
	}
}

let viewEngine = new ViewEngine();
describe('Class components', () => {
	it('Basic class component render', async () => {
		let viewBuilder = viewEngine.getBuilder(ClassComponent);
		let html = await viewBuilder.toString();
		expect(html).to.equal('<div></div>');
	});
	it('Multiple components', async () => {
		let viewBuilder = viewEngine.getBuilder(WrapClassComponent);
		let html = await viewBuilder.toString();
		expect(html).to.equal('<div><div><div></div></div><article><div></div></article></div>');
	});
	it('Lifecycle with replaced part of result', async () => {
		let viewBuilder = viewEngine.getBuilder(LifeCycle);
		let html = await viewBuilder.toString();
		expect(html).to.equal('<div>replacedtest</div>');
	});
	it('Component with database call', async () => {
		let viewBuilder = viewEngine.getBuilder(MockDatabaseCalls);
		let html = await viewBuilder.toString();
		expect(html).to.equal('<span>database data</span>');
	});
	it('Component with concurent database call', async () => {
		let viewBuilder = viewEngine.getBuilder(ConcurentCalls);
		let html = await viewBuilder.toString();
		expect(html).to.equal(
			'<div><span>database data</span><span>database data</span><span>database data</span><span>database data</span></div>'
		);
	});
});
