/* eslint-disable */

import { ViewEngine } from '../src';
import { expect } from 'chai';

ViewEngine.register('registertemplates');

import { Componenta } from './registertemplates/tsxcomponent';
import { JSComponenta } from './registertemplates/jsxcomponent';

let vE = new ViewEngine();
describe('Test register', () => {
	it('Import TSX', async () => {
		let vB = vE.getBuilder(Componenta);
		let result = await vB.toString();
		expect(result).to.equal('<div>tsx compnent</div>');
	});
	it('Import JSX', async () => {
		let vB = vE.getBuilder(JSComponenta);
		let result = await vB.toString();
		expect(result).to.equal('<span>jsx compnent</span>');
	});
});
