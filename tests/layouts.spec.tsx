import { expect } from 'chai';
import { View, ViewEngine, ViewEngineConfig } from '../src';

let layout = () => (
	<html>
		<head>
			<title>Test</title>
		</head>
		<body>
			<View.Placeholder />
		</body>
	</html>
);

let nextLayout = () => (
	<html>
		<head>
			<title>New layout</title>
		</head>
		<body>
			<View.Placeholder />
		</body>
	</html>
);

let body = () => <div>Tohle je tělo!</div>;

let config = {
	layout: layout,
} as ViewEngineConfig;

let viewEngine = new ViewEngine(config);
describe('Test layouts', () => {
	it('Layout in ViewEngine', async () => {
		let builder = viewEngine.getBuilder(body);
		let html = await builder.toString();
		expect(html).to.equal('<html><head><title>Test</title></head><body><div>Tohle je tělo!</div></body></html>');
	});
	it('Custom layout in ViewBuilder', async () => {
		let builder = viewEngine.getBuilder(body);
		builder.layout = nextLayout;
		let html = await builder.toString();
		expect(html).to.equal(
			'<html><head><title>New layout</title></head><body><div>Tohle je tělo!</div></body></html>'
		);
	});
});
