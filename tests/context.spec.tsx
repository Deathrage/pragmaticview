import { expect } from 'chai';
import { View, ViewEngine } from '../src/index';

let ContextReveal = (props: {}, context: any) => {
	return (
		<div>
			{Object.keys(context).map(key => (
				<span>{key + ': ' + context[key]}</span>
			))}
		</div>
	);
};

let FuncComponent = (props: any) => {
	return (
		<article>
			<ContextReveal />
		</article>
	);
};

class ClassContext extends View.Component {
	render() {
		return (
			<div>
				{Object.keys(this.context).map(key => (
					<span>{key + ': ' + this.context[key]}</span>
				))}
			</div>
		);
	}
}

let viewEngine = new ViewEngine();
describe('Context of rendering', () => {
	it('Context with Function component', async () => {
		let viewBuilder = viewEngine.getBuilder(FuncComponent, { path: 'foo/bar', title: 'The Book' });
		let html = await viewBuilder.toString();
		expect(html).to.equal('<article><div><span>path: foo/bar</span><span>title: The Book</span></div></article>');
	});
	it('Context with Class component', async () => {
		let viewBuilder = viewEngine.getBuilder(ClassContext, { path: 'foo/bar', title: 'The Book' });
		let html = await viewBuilder.toString();
		expect(html).to.equal('<div><span>path: foo/bar</span><span>title: The Book</span></div>');
	});
});
