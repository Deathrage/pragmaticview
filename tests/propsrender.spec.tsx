import { expect } from 'chai';
import { View, ViewEngine } from '../src/index';

let Component = (props: { className?: string; innerLayer?: boolean; innerText?: string }) => {
	return (
		<span className={props.className || ''}>
			{props.innerLayer ? <div></div> : ''}
			{props.innerText || ''}
		</span>
	);
};
let WrapComponentClass = () => {
	return <Component className="best-class" />;
};
let WrapComponentConditionalRender = () => {
	return <Component innerLayer={true} />;
};
let WrapComponentText = () => {
	return (
		<div>
			<Component innerText="hello" />
		</div>
	);
};
let ComponentWithPropsChildren = (props: any) => {
	return <article>{props.children}</article>;
};
let ChildrenWrap = () => {
	return (
		<div>
			<ComponentWithPropsChildren>
				<WrapComponentClass />
			</ComponentWithPropsChildren>
		</div>
	);
};

let viewEngine = new ViewEngine();
describe('Props rendering', () => {
	it('ClassName & condition false', async () => {
		let viewBuilder = viewEngine.getBuilder(WrapComponentClass);
		let html = await viewBuilder.toString();
		expect(html).to.equal('<span class="best-class"></span>');
	});
	it('Condition true', async () => {
		let viewBuilder = viewEngine.getBuilder(WrapComponentConditionalRender);
		let html = await viewBuilder.toString();
		expect(html).to.equal('<span><div></div></span>');
	});
	it('Inner text', async () => {
		let viewBuilder = viewEngine.getBuilder(WrapComponentText);
		let html = await viewBuilder.toString();
		expect(html).to.equal('<div><span>hello</span></div>');
	});
	it('Props children', async () => {
		let viewBuilder = viewEngine.getBuilder(ChildrenWrap);
		let html = await viewBuilder.toString();
		expect(html).to.equal('<div><article><span class="best-class"></span></article></div>');
	});
});
