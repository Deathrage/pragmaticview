/* eslint-disable */
import { ViewEngine } from '../src';
import * as path from 'path';

let viewEngine = new ViewEngine({
	beautifyOutput: false,
	logRenderTime: true,
});
ViewEngine.register(path.resolve(__dirname, '.'));

import Document from './document/document';

(async function() {
	let viewBuilder = viewEngine.getBuilder(Document, {foo: 'bar'});
	let result = await viewBuilder.toString();
	result = result.replace(/\n/gi, '');
	result = result.replace(/\r\n/gi, '');
	console.log(result);
})();

// import View, { ViewEngine, ViewEngineConfig } from '../src';

// let layout = () => <html>
// 	<head>
// 		<title>Test</title>
// 	</head>
// 	<body>
// 		<View.Placeholder/>
// 	</body>
// </html>;

// let body = () => <div>
// 	Tohle je tělo!
// </div>;

// let config = {
// 	layout: layout,
// } as ViewEngineConfig;

// let viewEngine = new ViewEngine(config);
// (async function() {
// 	let builder = viewEngine.getBuilder(body);
// 	let html = await builder.toString();
// 	debugger;
// })();
