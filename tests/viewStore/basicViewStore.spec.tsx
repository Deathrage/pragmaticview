import { expect } from 'chai';
import { View, Decorators, ViewEngine } from '../../src';

const viewEngine = new ViewEngine();
describe('Basic use of viewStore', () => {
	it('Should share information', async () => {
		let spy: string = null;
		interface VS {
			foo: string;
		}
		@Decorators.viewStore
		class Writer extends View.Component<any, any, VS> {
			onInit() {
				this.viewStore['foo'] = 'bar';
			}
			render() {
				return <div></div>;
			}
		}
		@Decorators.viewStore
		class Listener extends View.Component<any, any, VS> {
			onInit() {
				spy = this.viewStore['foo'];
			}
			render() {
				return <div></div>;
			}
		}
		const Wrap = () => (
			<div>
				<Writer />
				<Listener />
			</div>
		);
		await viewEngine.render(Wrap);
		expect(spy).to.equal('bar');
	});
});
