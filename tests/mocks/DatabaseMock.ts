/* eslint-disable */
export namespace DatabaseMock {
    export function callMock<T>(ms: number, response: T): Promise<T> {
        return new Promise(resolve => {
            setTimeout(()=>{resolve(response)}, ms)
        });
    }
}