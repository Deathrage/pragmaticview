/* eslint-disable no-unused-expressions */
import * as sinon from 'sinon';
import { expect, use as chaiUse } from 'chai';
import { View, ViewEngine } from '../src';
import sinonChai = require('sinon-chai');
chaiUse(sinonChai);

let viewEngine = new ViewEngine();
describe('Lifecycle of Class component', () => {
	it('should call onInit once', async () => {
		const spy = sinon.spy();
		class ClassComponent extends View.Component {
			async onInit() {
				spy();
			}
			render() {
				return <div />;
			}
		}
		let builder = viewEngine.getBuilder(ClassComponent);
		await builder.toString();
		expect(spy).to.have.been.calledOnce;
	});
	it('should call onPreRender once', async () => {
		const spy = sinon.spy();
		class ClassComponent extends View.Component {
			async onPreRender() {
				spy();
			}
			render() {
				return <div />;
			}
		}
		let builder = viewEngine.getBuilder(ClassComponent);
		await builder.toString();
		expect(spy).to.have.been.calledOnce;
	});
	it('should call OnRender once', async () => {
		const spy = sinon.spy();
		class ClassComponent extends View.Component {
			async onRender(html: string) {
				spy();
				return html;
			}
			render() {
				return <div />;
			}
		}
		let builder = viewEngine.getBuilder(ClassComponent);
		await builder.toString();
		expect(spy).to.have.been.calledOnce;
	});
	it('multilevel order of indirect components', async () => {
		// Spies
		const parentInit = sinon.spy();
		const parentPreRender = sinon.spy();
		const parentRender = sinon.spy();
		const childInit = sinon.spy();
		const childRender = sinon.spy();
		// Comps
		class Parent extends View.Component {
			async onInit() {
				parentInit();
			}
			async onPreRender() {
				parentPreRender();
			}
			async onRender(html: string) {
				parentRender();
				return html;
			}
			render() {
				return <div />;
			}
		}
		class Child extends View.Component {
			async onInit() {
				childInit();
			}
			async onRender(html: string) {
				childRender();
				return html;
			}
			render() {
				return <div />;
			}
		}
		const Wrap = () => (
			<Parent>
				<Child />
			</Parent>
		);

		let builder = viewEngine.getBuilder(Wrap);
		await builder.toString();
		sinon.assert.callOrder(parentInit, childInit, childRender, parentPreRender, parentRender);
	});
	it('multilevel order of direct components', async () => {
		// Spies
		const parentInit = sinon.spy();
		const parentPreRender = sinon.spy();
		const parentRender = sinon.spy();
		const childInit = sinon.spy();
		const childRender = sinon.spy();
		// Comps
		class Child extends View.Component {
			async onInit() {
				childInit();
			}
			async onRender(html: string) {
				childRender();
				return html;
			}
			render() {
				return <div />;
			}
		}
		class Parent extends View.Component {
			async onInit() {
				parentInit();
			}
			async onPreRender() {
				parentPreRender();
			}
			async onRender(html: string) {
				parentRender();
				return html;
			}
			render() {
				return <Child />;
			}
		}

		let builder = viewEngine.getBuilder(Parent);
		await builder.toString();
		sinon.assert.callOrder(parentInit, parentPreRender, childInit, childRender, parentRender);
	});

	it('multilevel order of mixed direct and indirect components', async () => {
		// Spies
		const parentInit = sinon.spy();
		const parentPreRender = sinon.spy();
		const parentRender = sinon.spy();
		const dirChildInit = sinon.spy();
		const dirChildRender = sinon.spy();
		const indirChildInit = sinon.spy();
		const indirChildRender = sinon.spy();
		// Comps
		class DirChild extends View.Component {
			async onInit() {
				dirChildInit();
			}
			async onRender(html: string) {
				dirChildRender();
				return html;
			}
			render() {
				return <div />;
			}
		}
		class IndirChild extends View.Component {
			async onInit() {
				indirChildInit();
			}
			async onRender(html: string) {
				indirChildRender();
				return html;
			}
			render() {
				return <div />;
			}
		}
		class Parent extends View.Component {
			async onInit() {
				parentInit();
			}
			async onPreRender() {
				parentPreRender();
			}
			async onRender(html: string) {
				parentRender();
				return html;
			}
			render() {
				return <DirChild />;
			}
		}
		const Wrap = () => (
			<Parent>
				<IndirChild />
			</Parent>
		);

		let builder = viewEngine.getBuilder(Wrap);
		await builder.toString();
		sinon.assert.callOrder(
			parentInit,
			indirChildInit,
			indirChildRender,
			parentPreRender,
			dirChildInit,
			dirChildRender,
			parentRender
		);
	});
});
