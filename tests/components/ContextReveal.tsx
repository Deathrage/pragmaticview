// eslint-disable-next-line
import { View } from '../../src/View';

export default function ContextReveal(props: {}, context: any) {
	return (
		<div>
			{Object.keys(context).map(key => <span>{key + ': ' + context[key]}</span>)}
		</div>
	);
};
