import { expect } from 'chai';
import { View, ViewEngine } from '../src/index';

// Mock components
let Component = () => {
	return <div>test</div>;
};
let WrapComponent = () => {
	return <Component />;
};
let WrapComponentWithDiv = () => {
	return (
		<div>
			<Component />
		</div>
	);
};
let ArrayOfDivs = () => {
	return [<div>foo</div>, <div>bar</div>];
};
let ArrayOfDivsInDivs = () => {
	return (
		<div>
			<ArrayOfDivs />
		</div>
	);
};
let ThreeDirectLayers = () => {
	return (
		<span>
			<WrapComponentWithDiv />
		</span>
	);
};
let ThreeIndirectLayers = () => {
	return (
		<article>
			three
			<div>
				two<span>one</span>
			</div>
		</article>
	);
};
let MixedComponent = () => {
	return (
		<body>
			<ArrayOfDivsInDivs />
			<WrapComponentWithDiv />
			<section>
				<ThreeIndirectLayers />
				<ThreeDirectLayers />
			</section>
		</body>
	);
};

// One engine is enough
let viewEngine = new ViewEngine();
describe('Basic rendering', () => {
	it('Single div', async () => {
		let viewBuilder = viewEngine.getBuilder(Component);
		let html = await viewBuilder.toString();
		expect(html).to.equal('<div>test</div>');
	});
	it('Component inside component', async () => {
		let viewBuilder = viewEngine.getBuilder(WrapComponent);
		let html = await viewBuilder.toString();
		expect(html).to.equal('<div>test</div>');
	});
	it('Component inside component and div', async () => {
		let viewBuilder = viewEngine.getBuilder(WrapComponentWithDiv);
		let html = await viewBuilder.toString();
		expect(html).to.equal('<div><div>test</div></div>');
	});
	it('Should render array of divs', async () => {
		let viewBuilder = viewEngine.getBuilder(ArrayOfDivs);
		let html = await viewBuilder.toString();
		expect(html).to.equal('<div>foo</div><div>bar</div>');
	});
	it('Should render array of divs in div', async () => {
		let viewBuilder = viewEngine.getBuilder(ArrayOfDivsInDivs);
		let html = await viewBuilder.toString();
		expect(html).to.equal('<div><div>foo</div><div>bar</div></div>');
	});
	it('Three indirect components', async () => {
		let viewBuilder = viewEngine.getBuilder(ThreeDirectLayers);
		let html = await viewBuilder.toString();
		expect(html).to.equal('<span><div><div>test</div></div></span>');
	});
	it('Three direct components', async () => {
		let viewBuilder = viewEngine.getBuilder(ThreeIndirectLayers);
		let html = await viewBuilder.toString();
		expect(html).to.equal('<article>three<div>two<span>one</span></div></article>');
	});
	it('Mixed cascade of ocmponents', async () => {
		let viewBuilder = viewEngine.getBuilder(MixedComponent);
		let html = await viewBuilder.toString();
		expect(html).to.equal(
			'<body><div><div>foo</div><div>bar</div></div><div><div>test</div></div><section><article>three<div>two<span>one</span></div></article><span><div><div>test</div></div></span></section></body>'
		);
	});
});
