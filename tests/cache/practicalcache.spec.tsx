/* eslint-disable no-unused-expressions */
import { Cache } from 'cache-with-resolver';
import { use as chaiUse, expect } from 'chai';
import * as sinon from 'sinon';
import { View, CacheConfig, Decorators, ViewEngine, ViewEngineConfig } from '../../src/index';
import sinonChai = require('sinon-chai');

chaiUse(sinonChai);

const { cache } = Decorators;

let viewEngine: ViewEngine = null;
let mockMap: Map<any, Cache> = null;

describe('Test caches in real scenarios', () => {
	beforeEach(() => {
		mockMap = new Map<any, Cache>();

		const cacheConfig: CacheConfig = {
			timeToLive: 0.5,
			cacheMap: mockMap,
		};
		const viewEngineConfig: ViewEngineConfig = {
			cacheConfig,
		};

		viewEngine = new ViewEngine(viewEngineConfig);
	});
	it('Should not re-render children', async () => {
		const spy = sinon.spy();
		const Child = () => {
			spy();
			return <div />;
		};
		@cache
		class CachedComponent extends View.Component {
			render() {
				return <Child />;
			}
		}
		const Wrap = () => <CachedComponent />;

		await viewEngine.getBuilder(Wrap).toString();
		await viewEngine.getBuilder(Wrap).toString();

		expect(spy).to.have.been.calledOnce;
	});
	it('Should not cache onRender', async () => {
		class Enumerator {
			static _next: number = 1;
			static get next() {
				const _num = Enumerator._next;
				Enumerator._next++;
				return String(_num);
			}
		}

		@cache
		class CachedComponent extends View.Component {
			render() {
				return <div>{Enumerator.next} + ##CHANGE##</div>;
			}
			async onRender(html: string) {
				return html.replace('##CHANGE##', Enumerator.next);
			}
		}
		const Wrap = () => <CachedComponent />;

		await viewEngine.getBuilder(Wrap).toString();
		const result = await viewEngine.getBuilder(Wrap).toString();
		expect(result).to.equal('<div>1 + 3</div>');
	});
	it('Should recognize keys', async () => {
		const onInitSpy = sinon.spy();
		const onRenderSpy = sinon.spy();

		interface Props {
			data: number;
		}

		@cache(props => `key${props.data}`)
		class CachedComponent extends View.Component<Props> {
			async onInit() {
				onInitSpy();
			}
			render() {
				return <div>{this.props.data}</div>;
			}
			async onRender(html: string) {
				onRenderSpy();
				return html;
			}
		}
		const Wrap = (props, context) => <CachedComponent data={context.key} />;

		await viewEngine.getBuilder(Wrap, { key: 1 }).toString();
		await viewEngine.getBuilder(Wrap, { key: 1 }).toString();
		await viewEngine.getBuilder(Wrap, { key: 2 }).toString();
		await viewEngine.getBuilder(Wrap, { key: 2 }).toString();

		expect(onInitSpy).to.have.been.calledTwice && expect(onRenderSpy.callCount).to.equal(4);
	});

	it('Should make render time shorter', async () => {
		@cache
		class CachedComponent extends View.Component {
			private data: any;

			async onInit() {
				this.data = new Promise(resolve => setTimeout(() => resolve('foo'), 500));
			}
			render() {
				return <div>{this.data}</div>;
			}
		}
		const Wrap = (props, context) => <CachedComponent />;

		const initTime = new Date().getTime();
		await viewEngine.getBuilder(Wrap).toString();
		await viewEngine.getBuilder(Wrap).toString();
		const doneTime = new Date().getTime();
		const spentTime = doneTime - initTime;

		expect(spentTime).to.be.below(999);
	});

	it('Parent should prevent child re-render', async () => {
		const childSpyConstructor = sinon.spy();
		const childSpyRender = sinon.spy();
		@cache(() => 'child', 0.1)
		class Child extends View.Component {
			constructor(...args) {
				childSpyConstructor();
				super(...args);
			}
			render() {
				childSpyRender();
				return <div />;
			}
		}
		@cache(() => 'parnet', 0.7)
		class Parent extends View.Component {
			render() {
				return <Child />;
			}
		}
		const Wrap = (props, context) => <Parent />;

		await viewEngine.getBuilder(Wrap).toString();
		// Sleep 0.3
		// Child's cache has worn out, but parent's cache is alive
		await new Promise(resolve => setTimeout(() => resolve(), 300));

		await viewEngine.getBuilder(Wrap).toString();

		expect(childSpyRender).to.have.been.calledOnce && expect(childSpyConstructor).to.have.been.calledOnce;
	});

	it('Should cache indirect children too', async () => {
		const childSpy = sinon.spy();
		class Child extends View.Component {
			async onInit() {
				await new Promise(resolve => setTimeout(resolve, 700));
				childSpy();
			}
			render() {
				return <div />;
			}
		}
		@cache
		class Parent extends View.Component {
			render() {
				return <div>{this.props.children}</div>;
			}
		}
		const Wrap = () => (
			<span>
				<Parent>
					<Child />
				</Parent>
			</span>
		);

		const initTime = new Date().getTime();
		await viewEngine.getBuilder(Wrap).toString();
		await viewEngine.getBuilder(Wrap).toString();
		const spentTime = new Date().getTime() - initTime;

		expect(childSpy).to.have.been.calledOnce && expect(spentTime).to.be.below(999);
	});
});
