import { Cache } from 'cache-with-resolver';
import { expect } from 'chai';

import { View, CacheConfig, Decorators, ViewEngine, ViewEngineConfig } from '../../src';

let viewEngine: ViewEngine = null;
let mockMap: Map<any, Cache> = null;

describe('It should test basic use of cache', () => {
	beforeEach(() => {
		mockMap = new Map<any, Cache>();

		const cacheConfig: CacheConfig = {
			timeToLive: 0.1,
			cacheMap: mockMap,
		};
		const viewEngineConfig: ViewEngineConfig = {
			cacheConfig,
		};

		viewEngine = new ViewEngine(viewEngineConfig);
	});
	it('Should create cache', async () => {
		@Decorators.cache
		class CompA extends View.Component {
			render() {
				return <div />;
			}
		}

		let builder = viewEngine.getBuilder(CompA);
		const result = await builder.toString();
		expect(viewEngine.cacheControl.get(CompA)).to.equal(result);
	});
	it('Should create cache that time outs', async () => {
		@Decorators.cache
		class ComB extends View.Component {
			render() {
				return <div />;
			}
		}

		let builder = viewEngine.getBuilder(ComB);
		await builder.toString();
		// sleep 1 sec
		await new Promise(resolve => setTimeout(() => resolve(), 1000));
		expect(viewEngine.cacheControl.has(ComB)).to.equal(false);
	});
	it('Should create cache with custom key', async () => {
		@Decorators.cache((props, context) => `key${context.key}`)
		class ComC extends View.Component {
			render() {
				return <div />;
			}
		}

		const context = { key: 5 };

		let builder = viewEngine.getBuilder(ComC, context);
		const result = await builder.toString();
		expect(viewEngine.cacheControl.get('key5')).to.equal(result);
	});
	it('Should create constructor key and custom ttl', async () => {
		@Decorators.cache(null, 1)
		class ComD extends View.Component {
			render() {
				return <div />;
			}
		}

		let builder = viewEngine.getBuilder(ComD);
		const result = await builder.toString();
		// sleep 0.7 sec
		await new Promise(resolve => setTimeout(() => resolve(), 0.7));
		expect(viewEngine.cacheControl.get(ComD)).to.equal(result);
	});
	it('Should create function-key and custom ttl', async () => {
		@Decorators.cache(() => 'key', 1)
		class ComE extends View.Component {
			render() {
				return <div />;
			}
		}

		let builder = viewEngine.getBuilder(ComE);
		const result = await builder.toString();
		// sleep 0.7 sec
		await new Promise(resolve => setTimeout(() => resolve(), 0.7));
		expect(viewEngine.cacheControl.get('key')).to.equal(result);
	});
});
